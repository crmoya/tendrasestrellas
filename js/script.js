jQuery(document).ready(function(){
    
    redimensiona();
    $(window).resize(function() {
            redimensiona();
    });
    
    $('.login').click(function(e){
        $("#login-form").submit();
    });
    $('.fixed').change(function(e) {
        var text = $(this).val();
        text = text.replace(',','.');
        if(text.length == 0 || text.length > 2){
            $(this).val(1);
        }else{
            if(!isNaN(text)){
                    var num = new Number(text);
                    num = num.toFixed(0);
                    $(this).val(num);
            }else{
                    $(this).val(1);
            }
        }
    });
   
    function redimensiona(){
        var alto = $("#cabecera").height();
       // $(".imagen_acerca").height($(".portada_text").height());  
       // var alt_img = $(".img_acerca").height();
       // var alt_cuadro =  $(".imagen_acerca").height();
        //$(".img_acerca").css("position","relative");
        //$(".img_acerca").css("top",(alt_cuadro-alt_img)/2+"px");
        $(".espaciosuperior").height(alto);	
     
        if($(this).width()<700){
                //$(".text").height(200);
                $(".otros_dicen").width(200);
                $(".inner").height(200);
        }
        else{
                //$(".text").height(350);
                $(".otros_dicen").width(300);
                $(".inner").height(300);
        }
    }
        
    $(".no_clickeable").bind("contextmenu",function(e){
        return false;
    });

    
});