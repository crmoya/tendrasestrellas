<?php

/**
 * This is the model class for table "costos_envio".
 *
 * The followings are the available columns in table 'costos_envio':
 * @property integer $regiones_id
 * @property integer $metodos_envio_id
 * @property string $costo
 */
class CostosEnvio extends CActiveRecord
{
	
	public $region;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CostosEnvio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'costos_envio';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('regiones_id, metodos_envio_id, costo', 'required'),
			array('regiones_id, metodos_envio_id', 'numerical', 'integerOnly'=>true),
			array('costo', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('regiones_id,region, metodos_envio_id, costo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(	
			'regiones' => array(self::BELONGS_TO, 'Regiones', 'regiones_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'regiones_id' => 'Regiones',
			'metodos_envio_id' => 'Metodos Envio',
			'costo' => 'Costo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('regiones_id',$this->regiones_id);
		$criteria->compare('metodos_envio_id',$this->metodos_envio_id);
		$criteria->compare('costo',$this->costo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function searchMetodo($id)
	{
		
		$criteria=new CDbCriteria;
	
		$criteria->compare('regiones_id',$this->regiones_id);
		$criteria->compare('metodos_envio_id',$id);
		$criteria->compare('costo',$this->costo,true);
		$criteria->with = array('regiones');
		$criteria->compare('regiones.nombre', $this->region, true );
	
		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
				'sort'=>array(
						'attributes'=>array(
								'region'=>array(
										'asc'=>'regiones.nombre',
										'desc'=>'regiones.nombre DESC',
								),
								'*',
						),
				),
		));
	}
}