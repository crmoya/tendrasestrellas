<?php

/**
 * This is the model class for table "productos_de_pedido".
 *
 * The followings are the available columns in table 'productos_de_pedido':
 * @property string $id
 * @property string $producto
 * @property integer $cantidad
 * @property integer $precio
 * @property integer $precio_por_mayor
 * @property string $categoria
 * @property integer $vigente
 */
class ProductosDePedido extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProductosDePedido the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'productos_de_pedido';
	}
	
	public function primaryKey()
	{
		return 'id';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('producto, cantidad, precio, categoria', 'required'),
			array('cantidad, precio, precio_por_mayor', 'numerical', 'integerOnly'=>true),
			array('id', 'length', 'max'=>22),
			array('producto, categoria', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, producto, pedido_id,cantidad, precio, precio_por_mayor, categoria', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'producto' => 'Producto',
			'cantidad' => 'Cantidad',
			'precio' => 'Precio',
			'precio_por_mayor' => 'Precio Por Mayor',
			'categoria' => 'Categoria',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('producto',$this->producto,true);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('precio',$this->precio);
		$criteria->compare('precio_por_mayor',$this->precio_por_mayor);
		$criteria->compare('categoria',$this->categoria,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
	
	public function searchPedido($id)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
	
		$criteria=new CDbCriteria;
	
		$criteria->compare('id',$this->id,true);
		$criteria->compare('producto',$this->producto,true);
		$criteria->compare('pedido_id',$id);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('precio',$this->precio);
		$criteria->compare('precio_por_mayor',$this->precio_por_mayor);
		$criteria->compare('categoria',$this->categoria,true);
	
		$cdp = new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
		));
		
		$cdp->pagination=false;
		return $cdp;
	}
}