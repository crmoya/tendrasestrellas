<?php

/**
 * This is the model class for table "productos".
 *
 * The followings are the available columns in table 'productos':
 * @property integer $id
 * @property string $nombre
 * @property integer $categorias_id
 * @property integer $precio
 * @property integer $costo
 * @property string $descripcion
 * @property integer $precio_por_mayor
 *
 * The followings are the available model relations:
 * @property Categorias $categorias
 * @property Pedidos[] $pedidoses
 */
class Productos extends CActiveRecord
{
	public $categoria;
	public $imagen;
	
        public function nextId(){
            $productos = Productos::model()->findAll(array('order'=>'id asc','condition'=>'id > :id and categorias_id = :cat','params'=>array(':id'=>$this->id,':cat'=>$this->categorias_id)));
            $producto = null;
            if(count($productos)>0){
                $producto = $productos[0];
            }
            if($producto != null){
                return $producto->id;
            }
            else{
                return -1;
            }
        }
        
        public function previousId(){
            $productos = Productos::model()->findAll(array('order'=>'id desc','condition'=>'id < :id and categorias_id = :cat','params'=>array(':id'=>$this->id,':cat'=>$this->categorias_id)));
            $producto = null;
            if(count($productos)>0){
                $producto = $productos[0];
            }
            if($producto != null){
                return $producto->id;
            }
            else{
                return -1;
            }            
        }
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Productos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'productos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, categorias_id, precio', 'required'),
			array('nombre', 'unique'),
			array('categorias_id, precio, costo, precio_por_mayor', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>100),
			array('imagen', 'file', 'types'=>Tools::getTiposArchivos(), 'allowEmpty' => true),
			array('descripcion', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('categoria,id, nombre, categorias_id, precio, costo, descripcion, precio_por_mayor', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'categorias' => array(self::BELONGS_TO, 'Categorias', 'categorias_id'),
			'pedidoses' => array(self::MANY_MANY, 'Pedidos', 'productos_pedidos(productos_id, pedidos_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombre' => 'Nombre',
			'categorias_id' => 'Categoría',
			'categoria' => 'Categoría',
			'precio' => 'Precio',
			'costo' => 'Costo',
			'descripcion' => 'Descripción',
			'precio_por_mayor' => 'Precio Por Mayor',
		);
	}
	
	public function getImagenes(){
            $arr = array();
            $id = $this->id;
            if($id==null) return $arr;
            $path = Yii::app()->basePath."/imagenes/productos/".$id;
            $dir_handle = @opendir($path);
            $i = 0;
            while ($file = readdir($dir_handle)) {
                if($file!="." && $file!=".."){
                    $arr[$i] = $file;
                    $i++;
                }
            }
            closedir($dir_handle);
            return $arr;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('t.nombre',$this->nombre,true);
		$criteria->compare('categorias_id',$this->categorias_id);
		$criteria->compare('precio',$this->precio);
		$criteria->compare('costo',$this->costo);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('precio_por_mayor',$this->precio_por_mayor);
		$criteria->with = array('categorias');
		$criteria->compare('categorias.nombre', $this->categoria, true );

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
					'attributes'=>array(
							'categoria'=>array(
									'asc'=>'categorias.nombre',
									'desc'=>'categorias.nombre DESC',
							),
							'*',
					),
			),
		));
	}
}