<?php

/**
 * This is the model class for table "clientes".
 *
 * The followings are the available columns in table 'clientes':
 * @property integer $id
 * @property string $direccion
 * @property integer $usuarios_id
 * @property integer $comunas_id
 * @property string $rut
 */
class Clientes extends CActiveRecord
{
    public $comuna;
    public $region;
    public $user;
    public $nombre;
    
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Clientes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'clientes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('direccion, usuarios_id, comunas_id', 'required'),
			array('usuarios_id, comunas_id', 'numerical', 'integerOnly'=>true),
                        array('rut', 'esRut'),
			array('direccion', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, direccion,region, rut,user,nombre,comuna,usuarios_id, comunas_id', 'safe', 'on'=>'search'),
		);
	}
        
        public function esRut($attribute,$params)
        {
            if($this->$attribute != ""){
                $rut = str_replace (".", "", $this->$attribute);
                $arr = explode("-", $rut);
                if(count($arr)!=2){
                    $this->addError($attribute, 'Ingrese un RUT válido, con guión y sin puntos.');
                }
                else{
                    $dv = $arr[1];
                    if(Tools::dv($arr[0])!=$dv){
                         $this->addError($attribute, 'RUT inválido');
                    }
                }
            }
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'usuarios' => array(self::BELONGS_TO, 'Usuarios', 'usuarios_id'),
                    'comunas' => array(self::BELONGS_TO, 'Comunas', 'comunas_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'direccion' => 'Direccion',
			'usuarios_id' => 'Usuarios',
                        'user'=>'Email',
                        'nombre'=>'Nombre',
			'comunas_id' => 'Comunas',
                        'region'=>'Región',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                //$criteria->with = array('comunas','usuarios');
                $criteria->join = " join comunas on comunas.id = t.comunas_id "
                        . "         join usuarios on usuarios.id = t.usuarios_id "
                        . "         join regiones on regiones.id = comunas.regiones_id ";
		$criteria->compare('id',$this->id);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('usuarios_id',$this->usuarios_id);
                $criteria->compare('rut',$this->rut,true);
		$criteria->compare('comunas.nombre',$this->comuna,true);
                $criteria->compare('regiones.nombre',$this->region,true);
                $criteria->compare('usuarios.user',$this->user,true);
                $criteria->compare('usuarios.nombre',$this->nombre,true);

		return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'sort'=>array(
                        'attributes'=>array(
                            'user'=>array(
                                'asc'=>'usuarios.user',
                                'desc'=>'usuarios.user DESC',
                            ),
                            'nombre'=>array(
                                'asc'=>'usuarios.nombre',
                                'desc'=>'usuarios.nombre DESC',
                            ),
                            'comuna'=>array(
                                'asc'=>'comunas.nombre',
                                'desc'=>'comunas.nombre DESC',
                            ),
                            'region'=>array(
                                'asc'=>'regiones.nombre',
                                'desc'=>'regiones.nombre DESC',
                            ),
                            '*',
                        ),
                    ),
		));
	}
}