<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class RegistrateForm extends CFormModel
{
	public $username;
        public $rut;
        public $direccion;
        public $comuna_id;
        public $region_id;
	public $password;
        public $repite_password;
        public $nombre;


	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
            return array(
                // username and password are required
                array('username,nombre,direccion, password,repite_password,comuna_id,region_id', 'required'),
                array('repite_password', 'compare', 'compareAttribute'=>'password', 'message'=>"Claves no coinciden"),
                array('rut', 'esRut'),
                array('username', 'email'),
            );
	}

        public function esRut($attribute,$params)
        {
            if($this->$attribute != ""){
                $rut = str_replace (".", "", $this->$attribute);
                $arr = explode("-", $rut);
                if(count($arr)!=2){
                    $this->addError($attribute, 'Ingrese un RUT válido, con guión y sin puntos.');
                }
                else{
                    $dv = $arr[1];
                    if(Tools::dv($arr[0])!=$dv){
                         $this->addError($attribute, 'RUT inválido');
                    }
                }
            }
        }
	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
            return array(
                'username'=>'Email',
                'nombre'=>'Nombre',
                'direccion'=>'Dirección',
                'region_id'=>'Región',
                'comuna_id'=>'Comuna',
                'password'=>'Clave',
                'repite_password'=>'Repite Clave',
            );
	}


}
