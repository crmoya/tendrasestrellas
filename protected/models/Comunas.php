<?php

/**
 * This is the model class for table "comunas".
 *
 * The followings are the available columns in table 'comunas':
 * @property integer $id
 * @property string $nombre
 * @property integer $ciudades_id
 *
 * The followings are the available model relations:
 * @property Clientes[] $clientes
 * @property Ciudades $ciudades
 */
class Comunas extends CActiveRecord
{
    public $region;
    
    public function listarDeRegion($region_id){
        $lista = $this->findAllByAttributes(array('regiones_id'=>$region_id),array('order'=>'nombre'));
        return $lista;
    }
    
    public function listar(){
        return $this->findAll(array('order'=>'nombre'));
    }
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Comunas the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'comunas';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                    array('nombre, regiones_id', 'required'),
                    array('ciudades_id', 'numerical', 'integerOnly'=>true),
                    array('nombre', 'length', 'max'=>100),
                    // The following rule is used by search().
                    // Please remove those attributes that should not be searched.
                    array('id, nombre, regiones_id', 'safe', 'on'=>'search'),
            );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                    'clientes' => array(self::HAS_MANY, 'Clientes', 'comunas_id'),
                    'regiones' => array(self::BELONGS_TO, 'Regiones', 'regiones_id'),
            );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
            return array(
                    'id' => 'ID',
                    'nombre' => 'Nombre',
                    'regiones_id' => 'Región',
                    'region' => 'Región',
            );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

            $criteria->compare('id',$this->id);
            $criteria->compare('t.nombre',$this->nombre,true);
            $criteria->compare('regiones_id',$this->regiones_id);
            $criteria->with = array('regiones');
            $criteria->compare('regiones.nombre', $this->region, true );

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'sort'=>array(
                        'attributes'=>array(
                            'ciudad'=>array(
                                'asc'=>'regiones.nombre',
                                'desc'=>'regiones.nombre DESC',
                            ),
                            '*',
                        ),
                    ),
            ));
    }
}