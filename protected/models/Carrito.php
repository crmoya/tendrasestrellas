<?php

/**
 * This is the model class for table "carrito".
 *
 * The followings are the available columns in table 'carrito':
 * @property integer $id
 * @property integer $cantidad
 * @property integer $usuarios_id
 * @property integer $productos_id
 *
 * The followings are the available model relations:
 * @property Usuarios $usuarios
 * @property Productos $productos
 */
class Carrito extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Carrito the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'carrito';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cantidad, usuarios_id, productos_id', 'required'),
			array('cantidad, usuarios_id, productos_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, cantidad, usuarios_id, productos_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarios' => array(self::BELONGS_TO, 'Usuarios', 'usuarios_id'),
			'productos' => array(self::BELONGS_TO, 'Productos', 'productos_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cantidad' => 'Cantidad',
			'usuarios_id' => 'Usuarios',
			'productos_id' => 'Productos',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('usuarios_id',$this->usuarios_id);
		$criteria->compare('productos_id',$this->productos_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}