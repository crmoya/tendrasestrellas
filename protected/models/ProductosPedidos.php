<?php

/**
 * This is the model class for table "productos_pedidos".
 *
 * The followings are the available columns in table 'productos_pedidos':
 * @property integer $productos_id
 * @property integer $pedidos_id
 */
class ProductosPedidos extends CActiveRecord
{
	
	public $producto;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProductosPedidos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'productos_pedidos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('productos_id, pedidos_id', 'required'),
			array('productos_id', 'ext.unique.UniqueAttributesValidator', 'with'=>'pedidos_id'),
			array('productos_id, pedidos_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('producto,cantidad,productos_id, pedidos_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'productos' => array(self::BELONGS_TO, 'Productos', 'productos_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'productos_id' => 'Producto',
			'pedidos_id' => 'Pedido',
			'productos' =>'Producto',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('productos_id',$this->productos_id);
		$criteria->compare('pedidos_id',$this->pedidos_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function searchProductos($id)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
	
		$criteria=new CDbCriteria;
	
		$criteria->compare('productos_id',$this->productos_id);
		$criteria->compare('pedidos_id',$id);
		$criteria->with = array('productos');
		$criteria->compare('productos.nombre', $this->producto, true );
		$criteria->compare('cantidad',$this->cantidad);
		
		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
				'sort'=>array(
						'attributes'=>array(
								'producto'=>array(
										'asc'=>'productos.nombre',
										'desc'=>'productos.nombre DESC',
								),
								'*',
						),
				),
		));
	}
}