<?php

/**
 * This is the model class for table "pedidos".
 *
 * The followings are the available columns in table 'pedidos':
 * @property integer $id
 * @property integer $clientes_id
 * @property string $fecha
 *
 * The followings are the available model relations:
 * @property Clientes $clientes
 * @property Productos[] $productoses
 */
class Pedidos extends CActiveRecord
{
	
	public $cliente;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Pedidos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pedidos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('clientes_id, fecha', 'required'),
			array('clientes_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id,cliente,vigente, clientes_id, fecha,cantidad_productos,costo_despacho,costo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'clientes' => array(self::BELONGS_TO, 'Clientes', 'clientes_id'),
			'productoses' => array(self::MANY_MANY, 'Productos', 'productos_pedidos(pedidos_id, productos_id)'),
			'metodos_envios' => array(self::BELONGS_TO, 'MetodosEnvio', 'metodos_envio_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'clientes_id' => 'Cliente',
			'fecha' => 'Fecha',
			'cliente'=>'Cliente',
			'metodo_envio'=>'Método de Envío',
		);
	}
	
	public function actualizar(){
	
		$connection=Yii::app()->db;
		$connection->active=true;
		$command=$connection->createCommand("
				select		sum(cantidad) as cant
				from		productos_de_pedido
				where		pedido_id = :pedido
				"
		);
		$pedido = $this->id;
		$command->bindParam(":pedido",$pedido,PDO::PARAM_INT);
		$dataReader=$command->query();
		$rows=$dataReader->readAll();
	
		$cantidad = 0;
		foreach($rows as $row){
			$cantidad = $row['cant'];
		}
		$precio = "precio";
		if($cantidad >= Tools::CANTIDAD_POR_MAYOR){
			$precio = "precio_por_mayor";
		}
	
		$command=$connection->createCommand("
				select		sum(cantidad*$precio) as costo
				from		productos_de_pedido
				where		pedido_id = :pedido
				"
		);
		$pedido = $this->id;
		$command->bindParam(":pedido",$pedido,PDO::PARAM_INT);
		$dataReader=$command->query();
		$rows=$dataReader->readAll();
		$connection->active=false;
		$command = null;
	
		$costo = 0;
		foreach($rows as $row){
		$costo = $row['costo'];
		}
		$connection->active=true;
		$command=$connection->createCommand("
				update 	pedidos
				set		cantidad_productos = :cantidad,
				costo = :costo
				where	id = :pedido
				"
		);
				$command->bindParam(":pedido",$pedido,PDO::PARAM_INT);
				$command->bindParam(":cantidad",$cantidad,PDO::PARAM_INT);
				$command->bindParam(":costo",$costo,PDO::PARAM_INT);
				$command->execute();
		$connection->active=false;
		$command = null;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('t.id',$this->id);
		$criteria->compare('clientes_id',$this->clientes_id);
                $criteria->compare('cantidad_productos',$this->cantidad_productos);
                $criteria->compare('costo_despacho',$this->costo_despacho);
                $criteria->compare('costo',$this->costo);
                if($this->vigente == ""){
                    $criteria->compare('vigente',true);
                }
		$criteria->compare('fecha',Tools::fixFecha($this->fecha),true);
                $criteria->join = 'JOIN clientes c ON t.clientes_id = c.id ';
                $criteria->join .= 'JOIN usuarios u ON c.usuarios_id = u.id ';
                $criteria->compare('u.nombre', $this->cliente, true );

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
					'attributes'=>array(
							'cliente'=>array(
									'asc'=>'clientes.nombre',
									'desc'=>'clientes.nombre DESC',
							),
							'*',
					),
			),
		));
	}
	
	protected function plataPrecio($data,$row)
	{
		return Tools::formateaPlata($data->precio);
	}
	
	protected function plataPrecioMayor($data,$row)
	{
		return Tools::formateaPlata($data->precio_por_mayor);
	}
	
	protected function gridDataColumn($data,$row)
	{
		return Tools::backFecha($data->fecha);
	}
}