<?php

/**
 * This is the model class for table "productos_destacados".
 *
 * The followings are the available columns in table 'productos_destacados':
 * @property integer $id
 * @property integer $productos_id
 *
 * The followings are the available model relations:
 * @property Productos $productos
 */
class ProductosDestacados extends CActiveRecord
{
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProductosDestacados the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function deleteOldest(){
		$destacados = ProductosDestacados::model()->findAll(array('order'=>'id'));
		foreach ($destacados as $destacado){
			$destacado->delete();
			break;
		}
	}
	
	public static function destacadoDeCategoria($categoria_id){
		$prod_id = -1;
		$connection=Yii::app()->db;
		$connection->active=true;
		$sql = "
			select 	p.id as pid
			from	productos as p,productos_destacados as d,categorias as c
			where	p.id = d.productos_id and
					c.id = p.categorias_id and
					c.id = :categoria
			";	
		$command=$connection->createCommand($sql);
		$command->bindParam(":categoria",$categoria_id,PDO::PARAM_INT);
		$dataReader=$command->query();
		foreach($dataReader as $row) { 
			$prod_id = $row['pid'];
			break;
		}
		$connection->active=false;
		$command = null;
		
		//si la categoría no tiene productos destacados, entonces entrego cualquier producto de la categoría
		if($prod_id == 1){
			$connection=Yii::app()->db;
			$connection->active=true;
			$sql = "
				select 	p.id as pid
				from	productos as p,categorias as c
				where	c.id = p.categorias_id and
						c.id = :categoria
				";	
			$command=$connection->createCommand($sql);
			$command->bindParam(":categoria",$categoria_id,PDO::PARAM_INT);
			$dataReader=$command->query();
			foreach($dataReader as $row) { 
				$prod_id = $row['pid'];
				break;
			}
			$connection->active=false;
			$command = null;
		}
		return $prod_id;
	}
	
	public static function cuantos(){
		$destacados = ProductosDestacados::model()->findAll();
		return count($destacados);
	}
	
	public static function getImagen($id){
		$existe = ProductosDestacados::model()->findByAttributes(array('productos_id'=>$id));
		if($existe != null)
			return Yii::app()->request->baseUrl.'/images/destacado.png';
		else 
			return Yii::app()->request->baseUrl.'/images/nodestacado.png';
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'productos_destacados';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('productos_id', 'required'),
			array('productos_id', 'unique'),
			array('productos_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, productos_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'productos' => array(self::BELONGS_TO, 'Productos', 'productos_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'productos_id' => 'Productos',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('productos_id',$this->productos_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}