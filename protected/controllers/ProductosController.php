<?php

class ProductosController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('borrar','index','view','create','update','admin','delete','borrar','exportar','eliminarImagen','destacar','disponibilidad','novedad'),
				'roles'=>array('administrador'),
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('view'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

        public function actionTest(){
            $files  = array('files'=>array(), 'dirs'=>array()); 
            $directories  = array(); 
            $root = Yii::app()->basePath."/imagenes/productos/11";
            $last_letter  = $root[strlen($root)-1]; 
            
            $root  = ($last_letter == '\\' || $last_letter == '/') ? $root : $root.DIRECTORY_SEPARATOR; 
              
            $directories[]  = $root; 
            $max = 0;
            while (sizeof($directories)) { 
              $dir  = array_pop($directories); 
              if ($handle = opendir($dir)) { 
                while (false !== ($file = readdir($handle))) { 
                  if ($file=="." || $file==".." || Tools::endsWith($file, ".svn") || Tools::endsWith($file, ".min")) { 
                      
                  }else{
                    echo $file;
                    if(is_numeric($file) && $file > $max){
                        $max = $file;
                    } 
                  }
                } 
                closedir($handle); 
              } 
            } 
            echo $max;
        }
	
	public function actionDestacar($id){
		$pd = ProductosDestacados::model()->findByAttributes(array('productos_id'=>$id));
		if($pd == null){
			$cantidad_destacados = count(Categorias::model()->findAll());
			if(ProductosDestacados::cuantos() == $cantidad_destacados){
				ProductosDestacados::deleteOldest();
			}
			$pd = new ProductosDestacados();
			$pd->productos_id = $id;
			if($pd->validate()){
				$pd->save();
			}
		}else{
			$pd->delete();
		}
		$this->redirect(CController::createUrl('//productos/admin'));
	}
        
        public function actionDisponibilidad($id){
		$pd = Productos::model()->findByPk($id);
		if($pd != null){
                    if($pd->disponible == 1){
                        $pd->disponible = 0;
                    }
                    else{
                        $pd->disponible = 1;
                    }
                    $pd->save();
		}
		$this->redirect(CController::createUrl('//productos/admin'));
	}
        
        public function actionNovedad($id){
		$pd = Novedades::model()->findByAttributes(array('productos_id'=>$id));
		if($pd != null){
                    $pd->delete();
		}
                else{
                    $pd = new Novedades();
                    $pd->productos_id = $id;
                    $pd->save();
                }
		$this->redirect(CController::createUrl('//productos/admin'));
	}
        
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionEliminarImagen()
	{
		$id = $_GET['id'];
		$i = $_GET['i'];
		if(file_exists(Yii::app()->basePath.'/imagenes/productos/'.$id.'/'.$i)){
			unlink(Yii::app()->basePath.'/imagenes/productos/'.$id.'/'.$i);
                        
                        echo "Imagen Eliminada";
		}else{
			echo "Error, reintente";
		}
                if(file_exists(YiiBase::getPathOfAlias("webroot").'/images/productos/'.$id.'/'.$i.'.min')){
                    unlink(YiiBase::getPathOfAlias("webroot").'/images/productos/'.$id.'/'.$i.'.min');
                }
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
            
		$model=new Productos;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Productos']))
		{
			$model->attributes=$_POST['Productos'];
			$model->disponible = 1;
			if($model->validate()){
				$model->imagen=CUploadedFile::getInstance($model,'imagen');
				if($model->imagen != null){
					$model->save();
					if(!is_dir(Yii::app()->basePath.'/imagenes/productos/'.$model->id)){
                                            mkdir(Yii::app()->basePath.'/imagenes/productos/'.$model->id);
					}
                                        if(!is_dir(YiiBase::getPathOfAlias("webroot").'/images/productos/'.$model->id)){
                                            mkdir(YiiBase::getPathOfAlias("webroot").'/images/productos/'.$model->id);
                                        }
					$siguiente = count($model->getImagenes()) + 1;
					$model->imagen->saveAs(Yii::app()->basePath.'/imagenes/productos/'.$model->id.'/'.$siguiente);
                                        Tools::addSelloAgua(Yii::app()->basePath.'/imagenes/productos/'.$model->id.'/'.$siguiente);
                                        Tools::resizeImgBig(Yii::app()->basePath.'/imagenes/productos/'.$model->id.'/'.$siguiente,Yii::app()->basePath.'/imagenes/productos/'.$model->id.'/'.$siguiente);
					Tools::resizeImg(Yii::app()->basePath.'/imagenes/productos/'.$model->id.'/'.$siguiente,YiiBase::getPathOfAlias("webroot").'/images/productos/'.$model->id.'/'.$siguiente.'.min');
                                        $this->redirect(array('view','id'=>$model->id));
				}else{
					$model->addError('imagen','Debe seleccionar una imagen');
				}
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Productos']))
		{
                    $model->attributes=$_POST['Productos'];
                    $model->disponible = $_POST['Productos']['disponible'];
                    if($model->validate()){
                        $model->save();
                        $model->imagen=CUploadedFile::getInstance($model,'imagen');
                        if($model->imagen != null){
                            if(!is_dir(Yii::app()->basePath.'/imagenes/productos/'.$model->id)){
                                mkdir(Yii::app()->basePath.'/imagenes/productos/'.$model->id);
                            }
                            if(!is_dir(YiiBase::getPathOfAlias("webroot").'/images/productos/'.$model->id)){
                                mkdir(YiiBase::getPathOfAlias("webroot").'/images/productos/'.$model->id);
                            }
                            $siguiente = $model->getNextImagenId()+1;
                            $model->imagen->saveAs(Yii::app()->basePath.'/imagenes/productos/'.$model->id.'/'.$siguiente);
                            Tools::addSelloAgua(Yii::app()->basePath.'/imagenes/productos/'.$model->id.'/'.$siguiente);
                            Tools::resizeImgBig(Yii::app()->basePath.'/imagenes/productos/'.$model->id.'/'.$siguiente,Yii::app()->basePath.'/imagenes/productos/'.$model->id.'/'.$siguiente);
                            Tools::resizeImg(Yii::app()->basePath.'/imagenes/productos/'.$model->id.'/'.$siguiente,YiiBase::getPathOfAlias("webroot").'/images/productos/'.$model->id.'/'.$siguiente.'.min');
                        }
                    }
                    if(count($model->getImagenes()) == 0){
                        $model->addError("imagen","El producto debe tener al menos una imagen");
                    }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$pedidos = ProductosDePedido::model()->findAllByAttributes(array('producto_id'=>$id,'vigente'=>1));
		if(count($pedidos)>0){
			
		}else{
			$destacados = ProductosDestacados::model()->findAllByAttributes(array('productos_id'=>$id));
			foreach($destacados as $destacado){
                            $destacado->delete();
			}
			$pedidos = ProductosPedidos::model()->deleteAllByAttributes(array('productos_id'=>$id));
			Tools::deleteDirectory(Yii::app()->basePath.'/imagenes/productos/'.$model->id."/");
                        Tools::deleteDirectory(YiiBase::getPathOfAlias("webroot").'/images/productos/'.$model->id.'/');
			$model->delete();
		}
		
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Productos');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Productos('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Productos']))
			$model->attributes=$_GET['Productos'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Productos the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Productos::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Productos $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='productos-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
