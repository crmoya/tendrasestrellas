<?php

class AdminController extends Controller
{
	/**
	 * Declares class-based actions.
	 */

	public function actions()
	{
		return array(
		// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
		),
		// page action renders "static" pages stored under 'protected/views/site/pages'
		// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
		),
		);
	}
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		if(Yii::app()->user->isGuest){
			$this->actionLogin();
		}
		else{
			if(Yii::app()->user->rol == 'administrador'){
                            $com = Comentarios::model()->findAllByAttributes(array('aceptado'=>0));
                            $count = 0;
                            $comentarios = "";
                            if($com != null){
                                $count = count($com);
                            }
                            if($count > 0){
                                if($count > 1){
                                    $comentarios = "(".$count." nuevos)";
                                }
                                if($count == 1){
                                    $comentarios = "(1 nuevo)";
                                }
                            }
                            $this->render("//admin/indexAdmin",array('nombre'=>Yii::app()->user->nombre,'comentarios'=>$comentarios));
			}
		}
	}
	
	public function actionLogin()
	{
		$model=new LoginForm;
	
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	
		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()){
				$this->actionIndex();
				Yii::app()->end();
			}
				
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}
	
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
	public function actionSubirPortada(){
		$model = new PortadaForm();
		if(isset($_POST['PortadaForm']))
		{		
			$model->attributes=$_POST['PortadaForm'];
			$model->imagen=CUploadedFile::getInstance($model,'imagen');
				
			if($model->validate()){
				if($model->imagen!=null){
					$model->imagen->saveAs(Yii::app()->basePath.'/imagenes/portada/portada.jpg');
					Yii::app()->user->setFlash('portadaMessage','Imagen de portada cambiada con éxito');
					$this->refresh();
				}else{
					//$model->addError('imagen','Debe seleccionar una imagen');
					Yii::app()->user->setFlash('portadaError','No se pudo subir la imagen de portada.');
					$this->refresh();
				}
				
			}	
		}
		$this->render('subirPortada',array('model'=>$model));
	}
        
        public function actionSubirFlyer(){
                $model = Flyer::model()->find();
                if($model == null){
                    $model = new Flyer();
                }		
		if(isset($_POST['Flyer']))
		{		
			$model->attributes=$_POST['Flyer'];
			$model->imagen=CUploadedFile::getInstance($model,'imagen');
                      	$model->show = $_POST['Flyer']['show'];
			if($model->validate()){
                           	if($model->imagen!=null){
					$model->imagen->saveAs(Yii::app()->basePath.'/imagenes/flyer.jpg');
					Yii::app()->user->setFlash('flyerMessage','Flyer cambiado con éxito');
				}else{
					//$model->addError('imagen','Debe seleccionar una imagen');
					Yii::app()->user->setFlash('flyerError','No se pudo subir la imagen del flyer.');
				}
				$model->save();
                                $this->refresh();
			}	
		}
		$this->render('subirFlyer',array('model'=>$model));
	}
        
        public function actionSubirAbout(){
		$model = new PortadaForm();
		if(isset($_POST['PortadaForm']))
		{		
			$model->attributes=$_POST['PortadaForm'];
			$model->imagen=CUploadedFile::getInstance($model,'imagen');
				
			if($model->validate()){
				if($model->imagen!=null){
					$model->imagen->saveAs(Yii::app()->basePath.'/imagenes/portada/about.jpg');
					Yii::app()->user->setFlash('portadaMessage','Imagen de about cambiada con éxito');
					$this->refresh();
				}else{
					//$model->addError('imagen','Debe seleccionar una imagen');
					Yii::app()->user->setFlash('portadaError','No se pudo subir la imagen de about.');
					$this->refresh();
				}
				
			}	
		}
		$this->render('subirPortada',array('model'=>$model));
	}
        
        public function actionSubirCondiciones(){
		$model = new PortadaForm();
		if(isset($_POST['PortadaForm']))
		{		
			$model->attributes=$_POST['PortadaForm'];
			$model->imagen=CUploadedFile::getInstance($model,'imagen');
				
			if($model->validate()){
				if($model->imagen!=null){
					$model->imagen->saveAs(Yii::app()->basePath.'/imagenes/portada/condiciones.jpg');
					Yii::app()->user->setFlash('portadaMessage','Imagen de conciones cambiada con éxito');
					$this->refresh();
				}else{
					//$model->addError('imagen','Debe seleccionar una imagen');
					Yii::app()->user->setFlash('portadaError','No se pudo subir la imagen de condiciones.');
					$this->refresh();
				}
				
			}	
		}
		$this->render('subirPortada',array('model'=>$model));
	}
        
        public function actionSubirPortada2(){
		$model = new PortadaForm();
		if(isset($_POST['PortadaForm']))
		{		
			$model->attributes=$_POST['PortadaForm'];
			$model->imagen=CUploadedFile::getInstance($model,'imagen');
				
			if($model->validate()){
				if($model->imagen!=null){
					$model->imagen->saveAs(Yii::app()->basePath.'/imagenes/portada/portada2.jpg');
					Yii::app()->user->setFlash('portadaMessage','Imagen de portada cambiada con éxito');
					$this->refresh();
				}else{
					//$model->addError('imagen','Debe seleccionar una imagen');
					Yii::app()->user->setFlash('portadaError','No se pudo subir la imagen de portada.');
					$this->refresh();
				}
				
			}	
		}
		$this->render('subirPortada',array('model'=>$model));
	}
        
	public function actionCambiarClave()
	{
		$form = new CambiarClaveForm();
		if (isset(Yii::app()->user->id))
		{
			if(isset($_POST['CambiarClaveForm']))
			{
				$form->attributes = $_POST['CambiarClaveForm'];
				if($form->validate())
				{
					$new_password = Usuarios::model()->findByPk(Yii::app()->user->id);
					if($new_password->clave != sha1($form->clave))
					{
						$form->addError('clave', "clave incorrecta");
					}
					else
					{
						if($form->nueva == $form->repita){
							$new_password->clave = sha1($form->nueva);
							if($new_password->save())
							{
								Yii::app()->user->setFlash('profileMessage',
										"Clave cambiada correctamente.");
							}
							else
							{
								Yii::app()->user->setFlash('profileMessage',
										"No se pudo cambiar la clave, inténtelo de nuevo más tarde.");
							}
							$this->refresh();
						}
						else{
							$form->addError('nueva', "claves nuevas no coinciden");
							$form->addError('repita', "claves nuevas no coinciden");
						}
	
					}
				}
			}
			$this->render('cambiarClave',array('model'=>$form));
		}
	}

	
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
			echo $error['message'];
			else
			$this->render('error', $error);
		}
	}


	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules()
	{
		return array(
		array('allow',
                    'actions'=>array('login','logout','error','index','test','cambiarClave','subirPortada','subirPortada2','subirFlyer','subirAbout','subirCondiciones'),
                    'roles'=>array('administrador'),
		),
                array('allow',
                    'actions'=>array('logout'),
                    'roles'=>array('cliente'),
		),
		array('allow',
                    'actions'=>array('index','login'),
                    'users'=>array('*'),
		),
		array('deny',  // deny all users
                    'users'=>array('*'),
		),
		);
	}
}