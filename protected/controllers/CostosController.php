<?php

class CostosController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('editar'),
				'roles'=>array('administrador'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionEditar()
	{
		if(isset($_POST['data'])){
			$arreglo = explode(";",$_POST['data']);
			$ok = true;
			foreach($arreglo as $dato){
				$datos = explode("_",$dato);
				if(count($datos)==3){
					$regiones_id = $datos[0];
					$metodos_envio_id = $datos[1];
					$costo = $datos[2];
					$costo_envio = CostosEnvio::model()->findByAttributes(array('regiones_id'=>$regiones_id,'metodos_envio_id'=>$metodos_envio_id));
					if($costo_envio != null){
						$costo_envio->costo = $costo;
						if($costo_envio->validate()){
							$costo_envio->save();
						}
						else{
							$ok = false;
						}
					}
					else{
						$ok = false;
					}
				}
			}
			if($ok) echo "ok";
			else echo "no";
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Clientes the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Clientes::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Clientes $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='clientes-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
