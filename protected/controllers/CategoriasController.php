<?php

class CategoriasController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete','exportar','getProductos'),
				'roles'=>array('administrador'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionGetProductos()
	{
		$categoria = $_POST['categoria_id'];
		$productos = Productos::model()->findAllByAttributes(array('categorias_id'=>$categoria));
		$productos=CHtml::listData($productos,'id','nombre');
	    foreach($productos as $value=>$name)
	    {
	        echo CHtml::tag('option',
	                   array('value'=>$value),CHtml::encode($name),true);
	    }
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Categorias;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Categorias']))
		{
			$model->attributes=$_POST['Categorias'];
			$model->imagen=CUploadedFile::getInstance($model,'imagen');
				
			if($model->validate()){
				//if($model->imagen!=null){
					$model->save();
					/*$model->imagen->saveAs(Yii::app()->basePath.'/imagenes/categorias/'.$model->id);
					Tools::addSelloAgua(Yii::app()->basePath.'/imagenes/categorias/'.$model->id);*/
					$this->redirect(array('view','id'=>$model->id));
				/*}else{
					$model->addError('imagen','Debe seleccionar una imagen');
				}*/
				
			}	
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		$prod_id = ProductosDestacados::destacadoDeCategoria($id);
		$model->producto_id = $prod_id;
		if(isset($_POST['Categorias']))
		{
			$model->attributes=$_POST['Categorias'];
			$model->imagen=CUploadedFile::getInstance($model,'imagen');
			
			if($model->validate()){
				$model->save();
				/*if($model->imagen !=null){
					$model->imagen->saveAs(Yii::app()->basePath.'/imagenes/categorias/'.$model->id);
					Tools::addSelloAgua(Yii::app()->basePath.'/imagenes/categorias/'.$model->id);
				}*/
				$model->producto_id = $_POST['Categorias']['producto_id'];
				//si la categoria tiene un destacado, entonces lo tengo que borrar para agregar el nuevo
				if($prod_id != -1){
					ProductosDestacados::model()->deleteAllByAttributes(array('productos_id'=>$prod_id));
				}
				$pd = new ProductosDestacados();
				$pd->productos_id = $model->producto_id;
				if($pd->validate()){
					$pd->save();
				}
				$this->redirect(array('view','id'=>$model->id));
			}
			
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$productos = Productos::model()->findAllByAttributes(array('categorias_id'=>$id));
		if(count($productos)>0){
		}else{
			$model = $this->loadModel($id);
			//unlink(Yii::app()->basePath.'/imagenes/categorias/'.$model->id);
			$model->delete();
		}
					
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Categorias');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Categorias('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Categorias']))
			$model->attributes=$_GET['Categorias'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Categorias the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Categorias::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Categorias $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='categorias-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
