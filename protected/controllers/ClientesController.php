<?php

class ClientesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete','exportar'),
				'roles'=>array('administrador'),
			),
                        array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','actualiza','cambiarClave','misPedidos','pedidoOK'),
				'roles'=>array('cliente'),
			),
                        array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('recupera'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Clientes;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Clientes']))
		{
			$model->attributes=$_POST['Clientes'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Clientes']))
		{
			$model->attributes=$_POST['Clientes'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

        
        public function actionCambiarClave()
	{
		$form = new CambiarClaveForm();
		if (isset(Yii::app()->user->id))
		{
			if(isset($_POST['CambiarClaveForm']))
			{
				$form->attributes = $_POST['CambiarClaveForm'];
				if($form->validate())
				{
					$new_password = Usuarios::model()->findByPk(Yii::app()->user->id);
					if($new_password->clave != sha1($form->clave))
					{
						$form->addError('clave', "clave incorrecta");
					}
					else
					{
						if($form->nueva == $form->repita){
							$new_password->clave = sha1($form->nueva);
							if($new_password->save())
							{
								Yii::app()->user->setFlash('profileMessage',
										"Clave cambiada correctamente.");
							}
							else
							{
								Yii::app()->user->setFlash('profileMessage',
										"No se pudo cambiar la clave, inténtelo de nuevo más tarde.");
							}
							$this->refresh();
						}
						else{
							$form->addError('nueva', "claves nuevas no coinciden");
							$form->addError('repita', "claves nuevas no coinciden");
						}
	
					}
				}
			}
			$this->render('cambiarClave',array('model'=>$form));
		}
	}

        public function actionRecupera()
	{
		$form = new CambiarClaveForm();
		if(isset($_POST['CambiarClaveForm']))
                {
                        $form->attributes = $_POST['CambiarClaveForm'];
                        $user = CHtml::encode($_GET['usuario']);
                        $hash = CHtml::encode($_GET['hash']);
                        if($hash == ""){
                            Yii::app()->user->setFlash('error',"ERROR: HASH INCORRECTO.");
                            $this->refresh();
                            Yii::app()->end;
                        }
                        
                        $form->clave = "1234";
                        if($form->validate())
                        {
                            if($form->nueva == $form->repita){
                                $user = Usuarios::model()->findByAttributes(array('user'=>$user,'hash'=>$hash));
                                if($user != null){
                                    $user->clave = sha1($form->nueva);
                                    $user->hash = "";
                                    if($user->save()){
                                        Yii::app()->user->setFlash('ok',"Clave cambiada correctamente.");
                                    }
                                    else{
                                        Yii::app()->user->setFlash('error',"No se pudo cambiar la clave, inténtelo de nuevo más tarde.");
                                    }
                                    $this->refresh();
                                    Yii::app()->end;
                                }
                                else{
                                    Yii::app()->user->setFlash('error',"ERROR: USUARIO Y HASH NO COINCIDEN.");
                                    $this->refresh();
                                    Yii::app()->end;
                                }
                            }
                            else{
                                Yii::app()->user->setFlash('error',"ERROR: CLAVES NO COINCIDEN.");
                                $this->refresh();
                                Yii::app()->end;
                            }					
                        }else{
                            Yii::app()->user->setFlash('error',"ERROR EN EL FORMULARIO:".CHtml::errorSummary($form));
                            $this->refresh();
                            Yii::app()->end;
                        }
                            
                }
                $this->render('recupera',array('model'=>$form));

	}
        
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
            $this->render('index');
	}
        
        
        public function actionActualiza(){
            $model=new RegistrateForm;

            $comuna_nombre = "";
            if(isset($_POST['RegistrateForm']))
            {
                $model->attributes=$_POST['RegistrateForm'];
                $comuna = Comunas::model()->findByPk($model->comuna_id);
                $comuna_nombre = $comuna->nombre;
                $model->password = "123";
                $model->repite_password = "123";
                if($model->validate()){
                    $usuario = Usuarios::model()->findByAttributes(array('user'=>$model->username));
                    $usuario->nombre = $model->nombre;
                    if($usuario->save()){
                        $cliente = Clientes::model()->findByAttributes(array('usuarios_id'=>$usuario->id));
                        $cliente->comunas_id = $model->comuna_id;
                        $cliente->direccion = $model->direccion;
                        $cliente->rut = $model->rut;
                        if($cliente->save()){
                            Yii::app()->user->setFlash('success',"Sus datos fueron correctamente cambiados.");
                            $this->refresh();                    
                        }
                        else{
                            Yii::app()->user->setFlash('error',"Error: sus datos no fueron actualizados, reintente.");
                            $this->refresh();
                        }
                    }
                    else{
                        Yii::app()->user->setFlash('error',CHtml::errorSummary($usuario));
                        $this->refresh();
                    }
                }
                else{
                    Yii::app()->user->setFlash('error',CHtml::errorSummary($model));
                    $this->refresh();
                }

            }else{
                $usuario_id = Yii::app()->user->id;
                $usuario = Usuarios::model()->findByPk($usuario_id);
                $cliente = Clientes::model()->findByAttributes(array('usuarios_id'=>$usuario_id));
                $comuna_id = $cliente->comunas_id;
                $comuna = Comunas::model()->findByPk($comuna_id);
                $region = $comuna->regiones;
                $region_id = $region->id;
                
                $model->comuna_id = $comuna_id;
                $model->rut = $cliente->rut;
                $model->region_id = $region_id;
                $model->direccion = $cliente->direccion;
                $model->nombre = $usuario->nombre;
                $model->username = $usuario->user;
                
                $comuna_nombre = $comuna->nombre;
            }
            // display the registrate form
            $this->render('cambiaDatos',array('model'=>$model,'comuna_nombre'=>$comuna_nombre));
        }

        public function actionMisPedidos(){
        }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Clientes('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Clientes']))
			$model->attributes=$_GET['Clientes'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Clientes the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Clientes::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Clientes $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='clientes-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
