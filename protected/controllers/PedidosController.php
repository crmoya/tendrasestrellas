<?php

class PedidosController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	
	public function behaviors()
	{
		return array(
				'eexcelview'=>array(
						'class'=>'ext.eexcelview.EExcelBehavior',
				),
		);
	}
	
	function actionExportar($id)
	{
		$nombre = $_GET['nombre'];
		$fecha = $_GET['fecha'];
		// generate a resultset
		$data = ProductosDePedido::model()->findAllByAttributes(array('pedido_id'=>$id));	
		$this->toExcel($data,
				array('producto','cantidad','precio','precio_por_mayor','categoria'),
				'Pedido '.$nombre.' '.$fecha,
				array()
		);
	}
	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete','exportar','editar','eliminar','agregar','getProductos'),
				'roles'=>array('administrador'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$productos = new ProductosDePedido();
		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'productos'=>$productos,
		));
	}
	
	public function actionEliminar($pedido_id,$producto_id)
	{
		$pedido_producto = ProductosPedidos::model()->findByAttributes(array('productos_id'=>$producto_id,'pedidos_id'=>$pedido_id));
		if($pedido_producto!=null){
			$pedido_producto->delete();
		}
		$productos = new ProductosDePedido();
		$this->render('update',array(
				'model'=>$this->loadModel($pedido_id),
				'productos'=>$productos,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Pedidos;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pedidos']))
		{
			$model->attributes=$_POST['Pedidos'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	public function actionEditar($pedido_id,$producto_id,$cantidad)
	{		
		$pedido_producto = ProductosPedidos::model()->findByAttributes(array('productos_id'=>$producto_id,'pedidos_id'=>$pedido_id));
		if($pedido_producto!=null){
			$pedido_producto->cantidad = $cantidad;
			if($pedido_producto->validate()){
				$pedido_producto->save();
				echo "Cantidad actualizada.";
			}
			else{
				echo "Error: reintente";
			}
		}
		else{
			echo "Error: reintente";
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$pedido = Pedidos::model()->findByPk($id);
		$pedido->actualizar();		
		
		$productos = new ProductosDePedido();
		$this->render('update',array(
			'model'=>$this->loadModel($id),
			'productos'=>$productos,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$pedido = $this->loadModel($id);
		if($pedido!=null){
			$pedido->vigente = 0;
			if($pedido->validate()){
				$pedido->save();
			}
		}
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
		
	public function actionAgregar($id)
	{
		$pedido = $this->loadModel($id);
		$producto = new Productos();
		$productos = new ProductosDePedido();
		
		if(isset($_POST['cantidad']) && isset($_POST['producto_id'])){
			$cantidad = $_POST['cantidad'];
			$producto_id = $_POST['producto_id'];
			
			$producto_pedido = new ProductosPedidos();
			$producto_pedido->productos_id = $producto_id;
			$producto_pedido->pedidos_id = $id;
			$producto_pedido->cantidad = $cantidad;
			if($producto_pedido->validate()){
				$producto_pedido->save();
				$this->render('update',array('model'=>$pedido,'productos'=>$productos));
				Yii::app()->end();
			}
			else{
				Yii::app()->user->setFlash('pedidoError',"Error: Producto no se pudo agregar al pedido. Verifique que producto no existe ya en el pedido.");
				$this->refresh();
				Yii::app()->end();
			}
		}
		
		$this->render('agregar',array('model'=>$pedido,'producto'=>$producto));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Pedidos');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Pedidos('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Pedidos']))
			$model->attributes=$_GET['Pedidos'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Pedidos the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Pedidos::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Pedidos $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pedidos-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
