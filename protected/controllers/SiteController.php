<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */

	public function actions()
	{
		return array(
		// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
		),
		// page action renders "static" pages stored under 'protected/views/site/pages'
		// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
		),
		);
                
	}
        
        public function actionFrecuentes(){
            $this->render('pages/frecuentes');
        }
        
        public function actionResetPassword(){
            $model=new LoginForm;
            // collect user input data
            if(isset($_POST['LoginForm']))
            {
                $secret = rand(1, 10000);
                $secretHash = sha1($secret);
                $model->attributes=$_POST['LoginForm'];
                $titulo = "RECUPERACIÓN DE CLAVE EN TENDRÁS ESTRELLAS";
                
                $user = Usuarios::model()->findByAttributes(array('user'=>$model->username));
                if($user!=null){
                    $mensaje = "<html>
                                    <head>
                                      <title>Recuperación clave</title>
                                    </head>
                                    <body>
                                        Has solicitado recuperar tu clave en TendrásEstrellas<br/>
                                        Por favor, si tú solicitaste esta recuperación
                                        haz click <a href='http://www.tendrasestrellas.cl".CController::createUrl("//clientes/recupera",array('hash'=>$secretHash,'usuario'=>$model->username))."'>acá</a> para que puedas crear una nueva clave<br/><br/>
                                        Por otro lado, si tú no has solicitado la recuperación, POR FAVOR obvía este mensaje.<br/><br/>
                                        
                                        Muchas gracias.<br/>
                                        Saludos!
                                    </body>
                                </html>";
                    $to_nombre = $model->username;
                    $to_email = $model->username;
                    $para = $to_email;
                    $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
                    $cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
                    $cabeceras .= 'To:  '.$to_nombre.'<'.$to_email.'>' . "\r\n";
                    $cabeceras .= 'From: TendrásEstrellas <contacto@tendrasestrellas.cl>' . "\r\n";
                    $cabeceras .= 'Reply-To: TendrásEstrellas <contacto@tendrasestrellas.cl>'."\r\n";
                    
                    $user->hash = $secretHash;
                    $user->save();
                    
                    mail($para, $titulo, $mensaje, $cabeceras);
                    Yii::app()->user->setFlash('ok','TE HEMOS ENVIADO UN CORREO CON LAS INSTRUCCIONES PARA REESTABLECER TU CLAVE');
                    $this->refresh();
                    Yii::app()->end;
                }
                else{
                    Yii::app()->user->setFlash('error','ERROR: EMAIL NO ENCONTRADO EN NUESTRAS BASES DE DATOS.');
                    $this->refresh();
                    Yii::app()->end;
                }              
            }
            // display the login form
            $this->render('//site/resetPassword',array('model'=>$model));
        }
        
        public function actionQuienes(){
            $this->render('pages/about');
        }
        
        public function actionCondiciones(){
            $this->render('pages/condiciones');
        }
	
	public function actionEliminarPedido(){
            $prod_id = -1;
            $cantidad = 1;
            if(isset($_POST['prod_id'])){
                $prod_id = $_POST['prod_id'];
            }
            
            if($prod_id > -1){
                $item = new PedidoItem($prod_id,0);
                
                if(!isset(Yii::app()->session['carro'])){
                    $carro = new Carro();
                }else{
                    $carro = Yii::app()->session['carro'];
                }
                $carro->eliminar($item);
                Yii::app()->session['carro'] = $carro;
                $total = $carro->num_productos();
                echo $total;
            }
            else{
                echo 0;
            }
               
        }
        
        public function actionAgregarPedido(){
            $prod_id = -1;
            $cantidad = 1;
            if(isset($_POST['prod_id'])){
                $prod_id = $_POST['prod_id'];
            }
            if(isset($_POST['cantidad'])){
                $cantidad = (int)$_POST['cantidad'];
                if($cantidad == 0){
                    $cantidad = 1;
                }
            }
            $producto = Productos::model()->findByPk($prod_id);
            $disponible = 0;
            if($producto != null){
                $disponible = $producto->disponible;
            }
            if($prod_id > -1 && is_int($cantidad) && $disponible == 1){
                $item = new PedidoItem($prod_id,$cantidad);
                if(!isset(Yii::app()->session['carro'])){
                    $carro = new Carro();
                }else{
                    $carro = Yii::app()->session['carro'];
                }
                $carro->agregar($item);
                Yii::app()->session['carro'] = $carro;
                $total = $carro->num_productos();
                echo $total;
            }
            else{
                echo 0;
            }
               
        }
        public function actionActualizarPedido(){
            $prod_id = -1;
            $cantidad = 1;
            if(isset($_POST['prod_id'])){
                $prod_id = $_POST['prod_id'];
            }
            if(isset($_POST['cantidad'])){
                $cantidad = (int)$_POST['cantidad'];
                if($cantidad == 0){
                    $cantidad = 1;
                }
            }
            if($prod_id > -1 && is_int($cantidad)){
                $item = new PedidoItem($prod_id,$cantidad);
                if(!isset(Yii::app()->session['carro'])){
                    $carro = new Carro();
                }else{
                    $carro = Yii::app()->session['carro'];
                }
                $carro->editar($item);
                
                Yii::app()->session['carro'] = $carro;
                $total = $carro->num_productos();
                echo $total;
                
            }
            else{
                echo 0;
            }
               
        }
        
        public function actionActualizarMetodo(){
            $metodo = "no_int";
            if(isset($_POST['metodo'])){
                $metodo = $_POST['metodo'];
            }
            if(isset($_POST['total'])){
                $total = $_POST['total'];
            }
            $region = "no_int";
            if(isset($_POST['region'])){
                $region = $_POST['region'];
            }
            //ver si alcanza el monto de despacho gratis
            if($total >= Tools::despacho_gratis){
                echo 0;
            }else{
                $costo_envio = CostosEnvio::model()->findByAttributes(array('regiones_id'=>$region,'metodos_envio_id'=>$metodo));
                if($costo_envio!=null){
                    echo $costo_envio->costo;   
                }
                else{
                    echo 0;
                }   
            }  
        }
	public function actionDynamic(){
            $this->render('pages/dynamic');
	}
	
	public function actionImagen($ruta)
	{
            $filename = $ruta;
            $path=Yii::getPathOfAlias('webroot.protected.imagenes') . '/';
            $file=$path.$filename;

            if (file_exists($file))
            {
                header('Content-Type: image/jpeg');
                readfile($file);
                exit;
            }
	}
        
        public function actionCuenta()
	{
            if(Yii::app()->user->isGuest){
                $this->actionLogin();
            }
            else{
                $this->actionMiCuenta();
            }
	}
        
        public function actionMiCuenta(){
            if(Yii::app()->user->rol == 'administrador'){
                $this->redirect(CController::createUrl("//admin/"));
            }
            if(Yii::app()->user->rol == 'cliente'){
                $this->redirect(CController::createUrl("//clientes/"));
            }
        }
        
        public function actionPedido()
	{
            $model=new PedidoForm;
            if(isset($_POST['PedidoForm']))
            {
                $model->attributes=$_POST['PedidoForm'];
                if($model->validate())
                {
                    if(isset(Yii::app()->session['carro'])){
                        $carro = Yii::app()->session['carro'];
                        $items = $carro->items();
                        
                        $para = Tools::mail_pedido;
                        $titulo = 'NUEVO PEDIDO EN TENDRÁSESTRELLAS';
                        $usuario = Usuarios::model()->findByPk(Yii::app()->user->id);
                        $para_cliente = $usuario->user;
                        $cantidad = $carro->num_productos();
                        $por_mayor = false;
                        if($cantidad >= 15){
                            $por_mayor = true;
                        }
                        
                        $mensaje_cliente = 
                               "<html>
                                <head>
                                  <title>Nuevo Pedido de ".$usuario->nombre." <".$usuario->user."></title>
                                </head>
                                <body>Felicitaciones ".$usuario->nombre."!! Muchas gracias por tu nuevo Pedido en TendrásEstrellas<br/>
                                    <p>El detalle de tu pedido es:</p>
                                  <table>
                                  <tr>
                                    <th>Producto</th><th>Precio</th><th>Cantidad</th>
                                  </tr>";
                        $mensaje = "<html>
                                <head>
                                  <title>Nuevo Pedido de ".$usuario->nombre." <".$usuario->user."></title>
                                </head>
                                <body>Nuevo Pedido de ".$usuario->nombre." <".$usuario->user."><br/><p>Los productos del pedido son:</p>
                                  <table>
                                  <tr>
                                    <th>Producto</th><th>Precio</th><th>Cantidad</th>
                                  </tr>";

                        $costo_pedido = 0;
                        $costo_envio = 0;
                        
                        $pedido = new Pedidos();
                        $pedido->fecha = date('Y-m-d');
                        $pedido->cantidad_productos = $cantidad;
                        foreach($items as $item){
                            $producto_id = $item->producto_id;
                            $producto = Productos::model()->findByPk($producto_id);
                            $cantidad = $item->cantidad;
                            $precio = $producto->precio;
                            if($por_mayor){
                                $precio =$producto->precio_por_mayor;
                            }
                            $costo_pedido+=$cantidad*$precio;
                            $mensaje = $mensaje."
                                <tr>
                                  <td>".CHtml::link($producto->nombre,Tools::URLSitio.CController::createUrl('//productos/')."/".$producto_id)
                                    . "</td><td>".Tools::formateaPlata($precio)."</td>"
                               . "<td>".$cantidad."</td>"
                                ."</tr>";
                            
                            $mensaje_cliente = $mensaje_cliente."
                                <tr>
                                  <td>".CHtml::link($producto->nombre,Tools::URLSitio.CController::createUrl('//productos/')."/".$producto_id)
                                    . "</a></td><td>".Tools::formateaPlata($precio)."</td>"
                               . "<td>".$cantidad."</td>"
                                ."</tr>";
                        }
                        $mensaje = $mensaje." 
                                <tr>
                                  <td colspan='2'>TOTAL DE PRODUCTOS</td>
                                  <td>".$pedido->cantidad_productos."</td>
                                </tr>
                                ";
                        $metodo_envio_id = $model->metodo_envio;
                        $metodo = MetodosEnvio::model()->findByPk($metodo_envio_id);
                        $cliente = Clientes::model()->findByAttributes(array('usuarios_id'=>$usuario->id));
                        $pedido->clientes_id = $cliente->id;
                        $comuna = Comunas::model()->findByPk($cliente->comunas_id);
                        $region = $comuna->regiones;
                        $region_id = $region->id;
                        $costoenvio = CostosEnvio::model()->findByAttributes(array('regiones_id'=>$region_id,'metodos_envio_id'=>$metodo_envio_id));
                        if($costoenvio != null){
                            $costo_envio = $costoenvio->costo;
                        }
                        if($costo_pedido > 0 && $costo_envio > 0){
                            
                            //se verifica envío gratuito
                            if($costo_pedido >= Tools::despacho_gratis){
                                $costo_envio = 0;
                            }
                            $pedido->costo_despacho = $costo_envio;
                            $pedido->costo = $costo_pedido;
                            $pedido->metodos_envio_id = $metodo_envio_id;
                            $pedido->vigente = true;
                            $pedido->save();
                            
                            foreach($items as $item){
                                $pedido_prod_id = $item->producto_id;
                                $pedido_prod_cant = $item->cantidad;
                                $prod_pedido = new ProductosPedidos();
                                $prod_pedido->pedidos_id = $pedido->id;
                                $prod_pedido->productos_id = $pedido_prod_id;
                                $prod_pedido->cantidad = $pedido_prod_cant;
                                $prod_pedido->save();
                            }
                            
                            $costo_total = $costo_pedido + $costo_envio;
                            $mensaje = $mensaje."</table>"
                                    . "<br/>"
                                    . "El costo del pedido es: ".Tools::formateaPlata($costo_pedido)."<br/>"
                                    . "El costo del envío es: ".Tools::formateaPlata($costo_envio)." y será enviado a través de ".$metodo->nombre."<br/>"
                                    . "El total de la transferencia es de: ".Tools::formateaPlata($costo_total)."<br/>"
                                    . "La dirección de envío es: ".$cliente->direccion.", Comuna: ".$comuna->nombre.", Región: ".$region->nombre."<br/>"
                                    . "</body></html>";
                            
                            $mensaje_cliente = $mensaje_cliente."</table>"
                                    . "<br/>"
                                    . "El costo de tu pedido es: ".Tools::formateaPlata($costo_pedido)."<br/>"
                                    . "El costo del envío es: ".Tools::formateaPlata($costo_envio)." y será enviado a través de ".$metodo->nombre."<br/>"
                                    . "El costo total del pedido es de: ".Tools::formateaPlata($costo_total)."<br/>"
                                    . "La dirección de envío es: ".$cliente->direccion.", Comuna: ".$comuna->nombre.", Región: ".$region->nombre."<br/><br/>"
                                    . "Te confirmaré a la brevedad el stock, la cuenta para el pago y la fecha de despacho.<br/>"
                                    . "Muchas gracias!.<br/><br/>"
                                    . "María José Jara Barquín<br/>"
                                    . "Dueña/Diseñadora de Tendrás Estrellas<br/>"
                                    . "</body></html>";
                            $to_nombre = "Administración Pedidos";
                            $to_email = Tools::mail_pedido;
                            $to_nombre_cliente = $usuario->nombre;
                            $to_email_cliente = $usuario->user;
                            $para = $to_email;
                            $para_cliente = $to_email_cliente;
                            $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
                            $cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
                            $cabeceras .= 'To:  '.$to_nombre.'<'.$to_email.'>' . "\r\n";
                            $cabeceras .= 'From: TendrásEstrellas <contacto@tendrasestrellas.cl>' . "\r\n";
                            $cabeceras .= 'Reply-To: '.$para_cliente . "\r\n";
                            $cabeceras_cliente  = 'MIME-Version: 1.0' . "\r\n";
                            $cabeceras_cliente .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
                            $cabeceras_cliente .= 'To:  '.$to_nombre_cliente.'<'.$to_email_cliente.'>' . "\r\n";
                            $cabeceras_cliente .= 'From: TendrásEstrellas <contacto@tendrasestrellas.cl>' . "\r\n";
                            mail($para, $titulo, $mensaje, $cabeceras);
                            mail($para_cliente, $titulo, $mensaje_cliente, $cabeceras_cliente);
                            
                            
                            $carro->vaciar();
                            Yii::app()->session['carro'] = $carro;
                            $this->redirect('pedidoOK');
                        }
                        else{
                            Yii::app()->user->setFlash('errorPedido','Ups, algo fue mal con el cálculo del costo de tu envío... Por favor reintenta.');
                            $this->refresh();
                        }
                        
                        
                    }else{
                        Yii::app()->user->setFlash('errorPedido','No se pudo procesar tu carro de compras. Por favor reintenta.');
                        $this->refresh();
                    } 

                    Yii::app()->user->setFlash('errorPedido','Ocurrió un error al enviar tu pedido, por favor reintenta.');
                    $this->refresh();
                }
            }
            $this->render('pedidos',array('model'=>$model));
            
	}
        
        public function actionPedidoOK(){
            $this->render('pedidoOK');
        }
        
	public function actionImagenPublica($ruta)
	{
		$filename = $ruta;
		$path=Yii::getPathOfAlias('webroot.protected.imagenes') . '/';
		$file=$path.$filename;
	
		if (file_exists($file))
		{
			header('Content-Type: image/jpeg');
			readfile($file);
			exit;
		}
	}
	
	public function actionConfigureRoles(){


		$record=AuthAssignment::model()->deleteAll();
		$record=AuthItemChild::model()->deleteAll();
		$record=AuthItem::model()->deleteAll();
		
		$auth=Yii::app()->authManager;

		$auth->createOperation('configureRoles','configure app roles');

		$role=$auth->createRole('administrador');
		$role->addChild('configureRoles');
		$role=$auth->createRole('operativo');
		$role=$auth->createRole('gerencia');
		$auth->assign('administrador',1);
		$this->render("//admin/indexAdmin",array('nombre'=>Yii::app()->user->nombre));

	}
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
            $this->actionMain();

	}

	public function actionAddComment(){
		$nombre = $_POST['nombre'];
		$comentario = $_POST['comentario'];
                $email = $_POST['email'];
		$comment = new Comentarios();
		$comment->nombre = $nombre;
		$comment->comentario = $comentario;
                $comment->email = $email;
		$comment->aceptado = 0;
		
		if($comment->validate()){
			$comment->save();
                        $mensaje = "<html>
                                    <head>
                                      <title>Nuevo Comentario</title>
                                    </head>
                                    <body>
                                        Hola, <br/>
                                        Has recibido un nuevo comentario.
                                        De: $comment->nombre <$comment->email><br/>
                                        Mensaje: $comment->comentario<br/><br/>
                                        Para aceptarlo/rechazarlo inicia sesión en tu cuenta de administrador <a href='http://www.tendrasestrellas.cl".CController::createUrl('//site/cuenta')."'>Inicia Sesión</a><br/><br/>    
                                        Muchas gracias.<br/>
                                        Saludos!
                                    </body>
                                </html>";
                        $to_nombre = "TENDRÁS ESTRELLAS";
                        $to_email = "pedido@tendrasestrellas.cl";
                        $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
                        $cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
                        $cabeceras .= 'To:  '.$to_nombre.'<'.$to_email.'>' . "\r\n";
                        $cabeceras .= 'From: TendrásEstrellas <contacto@tendrasestrellas.cl>' . "\r\n";
                        $cabeceras .= 'Reply-To: TendrásEstrellas <contacto@tendrasestrellas.cl>'."\r\n";

                        mail($to_email, "NUEVO COMENTARIO", $mensaje, $cabeceras);
			echo "Muchas gracias por su comentario.";
		}
		else{
			//echo CHtml::errorSummary($comment);
			echo "No se pudo guardar comentario, por favor reintente";
		}
		
	}
	public function actionMain()
	{
		$categorias = Categorias::model()->findAll();
		$model = new Categorias();
                $productos = Novedades::model()->findAll();
                $this->render('pages/main',array('categorias'=>$categorias,'model'=>$model,'novedades'=>$productos));
	}
	
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
			echo $error['message'];
			else
			$this->render('error', $error);
		}
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

/**
	 * Displays the contact page
	 */
	public function actionTest()
	{
		$model=new TestForm();
		if(isset($_POST['TestForm']))
		{
			$model->attributes=$_POST['TestForm'];
			if($model->validate())
			{
				
			}
		}
		$this->render('test',array('model'=>$model));
	}
        
        public function actionLogin()
	{
            $model=new LoginForm;

            // if it is ajax validation request
            if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
            {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            // collect user input data
            if(isset($_POST['LoginForm']))
            {
                $model->attributes=$_POST['LoginForm'];
                // validate user input and redirect to the previous page if valid
                if($model->validate() && $model->login()){
                    $current = Yii::app()->controller->getAction()->getId();
                    if($current == "cuenta"){
                        $this->actionMiCuenta();
                    }
                    else if($current == "pedido"){
                        $this->actionMiPedido();
                    }
                    else{
                        $this->actionIndex();
                    }
                    Yii::app()->end();
                }

            }
            // display the login form
            $this->render('login',array('model'=>$model));
	}
	
	public function actionRegistrate()
	{
            $model=new RegistrateForm;

            if(isset($_POST['RegistrateForm']))
            {
                $model->attributes=$_POST['RegistrateForm'];
                if($model->validate()){
                    $usuario = new Usuarios();
                    $usuario->user = $model->username;
                    $usuario->clave = sha1($model->password);
                    $usuario->nombre = $model->nombre;
                    $usuario->rol = "cliente";
                    
                    if($usuario->save()){
                        $cliente = new Clientes();
                        $cliente->comunas_id = $model->comuna_id;
                        $cliente->rut = $model->rut;
                        $cliente->direccion = $model->direccion;
                        $cliente->usuarios_id = $usuario->id;
                        $auth=Yii::app()->authManager;
                        $auth->assign('cliente',$usuario->id);
                        if($cliente->save()){
                            $mensaje = "Felicitaciones! tu registro en TendrásEstrellas ha sido exitoso.\nTu clave es: '".$model->password."'.\nYa puedes comenzar a hacer tus pedidos.\nRecuerda que puedes acceder a precios por mayor comprando más de 15 productos del catálogo.\n\nMuchas gracias, Cordialmente\nTendrásEstrellas";
                            $cabeceras =    'From: contacto@tendrasestrellas.cl' . "\r\n" .
                                            'Reply-To: contacto@tendrasestrellas.cl' . "\r\n";
                            mail($usuario->user,"Registro en TendrásEstrellas",$mensaje,$cabeceras);
                            $_identity=new UserIdentity($usuario->user,$model->password);
                            $_identity->authenticate();
                            if($_identity->errorCode===UserIdentity::ERROR_NONE){
                                Yii::app()->user->login($_identity,0);
                            }
                            $this->render('registroOK');
                        }
                        else{
                            $this->render('registroNO');
                        }
                    }
                    else{
                        $this->render('registroNO');
                    }
                }

            }
            // display the registrate form
            $this->render('registrate',array('model'=>$model));
	}
        
        public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules()
	{
		return array(
		array('allow',
                    'actions'=>array('actualizarMetodo','frecuentes','condiciones','registrate','eliminarPedido','actualizarPedido','cuenta','login','logout','error','index','test','dynamic','imagenPublica','addComment','agregarPedido','quienes','pedidoOK','resetPassword'),
                    'users'=>array('*'),
		),
		array('allow',
                    'actions'=>array('cambiarClave','logout'),
                    'users'=>array('@'),
		),
		array('allow',
                    'actions'=>array('configureRoles','imagen'),
                    'roles'=>array('administrador'),
		),
                array('allow',
                    'actions'=>array('miCuenta','miPedido','pedido'),
                    'roles'=>array('cliente'),
		),
		array('deny',  // deny all users
                    'users'=>array('*'),
		),
		);
	}
}