<?php

class MetodosEnvioController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete','exportar'),
				'roles'=>array('administrador'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$costos = new CostosEnvio();
		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'costos'=>$costos,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MetodosEnvio;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MetodosEnvio']))
		{
			$model->attributes=$_POST['MetodosEnvio'];
			$model->imagen=CUploadedFile::getInstance($model,'imagen');
			if($model->validate()){
				if($model->imagen != null){
					$model->save();
					$model->imagen->saveAs(Yii::app()->basePath.'/imagenes/metodos/'.$model->id);
					$regiones = Regiones::model()->findAll();
					foreach($regiones as $region){
						$costo = new CostosEnvio();
						$costo->costo = 0;
						$costo->metodos_envio_id = $model->id;
						$costo->regiones_id = $region->id;
						if($costo->validate()){
							$costo->save();
						}
					}
					$this->redirect(array('view','id'=>$model->id));
				}
				else{
					$model->addError('imagen','Debe seleccionar una imagen');
				}
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$costos = new CostosEnvio();
		
		if(isset($_POST['MetodosEnvio']))
		{
			$model->attributes=$_POST['MetodosEnvio'];
			$model->imagen=CUploadedFile::getInstance($model,'imagen');
			
			$model->save();
			if($model->imagen != null){
				$model->imagen->saveAs(Yii::app()->basePath.'/imagenes/metodos/'.$model->id);
			}
			
			Yii::app()->user->setFlash('costoMessage',"Costos de Envío modificados correctamente.");
			$this->refresh();
			Yii::app()->end();
		}

		$this->render('update',array(
			'model'=>$model,
			'costos'=>$costos
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		CostosEnvio::model()->deleteAllByAttributes(array('metodos_envio_id'=>$model->id));
		if(file_exists(Yii::app()->basePath.'/imagenes/metodos/'.$model->id))
			unlink(Yii::app()->basePath.'/imagenes/metodos/'.$model->id);
		$model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('MetodosEnvio');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new MetodosEnvio('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MetodosEnvio']))
			$model->attributes=$_GET['MetodosEnvio'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MetodosEnvio the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MetodosEnvio::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MetodosEnvio $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='metodos-envio-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
