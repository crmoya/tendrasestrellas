<?php
class CustomButtonColumn extends CButtonColumn
{
    protected function renderButton($id, $button, $row, $data)
    {
        $button['imageUrl'] = Productos::getImagenDisponibilidad($data->id);
        parent::renderButton($id, $button, $row, $data);
    }
}