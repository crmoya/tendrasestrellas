<?php
class Tools{
	
        const FaceBook = "https://www.facebook.com/tendrasestrellas";
        const URLSitio = "http://www.tendrasestrellas.cl";
        const por_mayor = 15;
        const mail_pedido = "pedido@tendrasestrellas.cl";
        
        const despacho_gratis = 35000;
    
        public static function endsWith($haystack, $needle){
            return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
        }
        
        public static function dv($r){
            $s=1;
            for($m=0;$r!=0;$r/=10)
                $s=($s+$r%10*(9-$m++%6))%11;
            return chr($s?$s+47:75);
        }
        
        
        public static function deleteDirectory($dirPath) {
            if(file_exists($dirPath)){
                if (! is_dir($dirPath)) {throw new InvalidArgumentException("$dirPath must be a directory");}
                if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {$dirPath .= '/'; }
                $files = glob($dirPath . '*', GLOB_MARK);
                foreach ($files as $file) {
                    if (is_dir($file)) { self::deleteDirectory($file);} 
                    else { unlink($file); }
                }
                rmdir($dirPath);               
            }            
        }
                    
        /*
        public static function deleteDirectory($path) {
            // Normalise $path.
            $path = rtrim($path, '/') . '/';
            // Remove all child files and directories.
            $items = glob($path . '*');
            foreach($items as $item) {
                is_dir($item) ? deleteDirectory($item) : unlink($item);
            }
            // Remove directory.
            rmdir($path);
        }
        
        public static function deleteDirectory($dir){
            if (is_dir($dir)) {
              $objects = scandir($dir);
              foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                  if (filetype($dir."/".$object) == "dir") rmdir($dir."/".$object); else unlink($dir."/".$object);
                }
              }
              reset($objects);
              rmdir($dir);
            }
        }*/
        
        public static function ultimoIndexOf($pajar,$aguja){
            $pos = -1;
            $last = 0;
            do{
                $last = $pos;
                $pos = strpos($pajar, $aguja, $pos+1);
            }while($pos != false && is_numeric($pos));
            return $last;
        }
        
	public static function addSelloAgua($rutaImagen){
		// Cargar la estampa y la foto para aplicarle la marca de agua
		$estampa = imagecreatefrompng('images/selloAgua.png');
		$im = imagecreatefromjpeg($rutaImagen);
		
		// Establecer los márgenes para la estampa y obtener el alto/ancho de la imagen de la estampa
		$margen_dcho = 5;
		$margen_inf = 5;
		$sx = imagesx($estampa);
		$sy = imagesy($estampa);
		
		// Copiar la imagen de la estampa sobre nuestra foto usando los índices de márgen y el
		// ancho de la foto para calcular la posición de la estampa. 
		imagecopy($im, $estampa, imagesx($im) - $sx - $margen_dcho, imagesy($im) - $sy - $margen_inf, 0, 0, imagesx($estampa), imagesy($estampa));
		
		// Imprimir y liberar memoria
		imagejpeg($im,$rutaImagen);
		imagedestroy($im);
	}
     	
	public static function getTiposArchivos(){
		return "jpg, gif, png, jpeg";
	}
	
	public static function fixFecha($fecha){
		$fechaArr = explode("/", $fecha);
		if(count($fechaArr)==3) return $fechaArr[2]."-".$fechaArr[1]."-".$fechaArr[0];
		else return "";
	}
	
	public static function backFecha($fecha){
		$fechaArr = explode("-", $fecha);
		if(count($fechaArr)==3) return $fechaArr[2]."/".$fechaArr[1]."/".$fechaArr[0];
		else return "";
	}
	
	public static function formateaPlata($valor){
		$formatter = new Formatter();
		return "$ ".$formatter->formatNumber($valor);
	}
        
        public static function formateaVigencia($valor){
		return ($valor == true)?"VIGENTE":"NO VIGENTE";
	}
	
	public static function disponible($dis){
		if($dis){
			return "DISPONIBLE";
		}else{
			return "NO DISPONIBLE";
		}
	}
	
	const CANTIDAD_POR_MAYOR = 15;
        
        public static function resizeImg($img,$rutaMiniatura){
            $file = $img;
            $width = 400;

            // Ponemos el . antes del nombre del archivo porque estamos considerando que la ruta está a partir del archivo thumb.php
            $file_info = getimagesize($file);
            // Obtenemos la relación de aspecto
            $ratio = $file_info[0] / $file_info[1];

            // Calculamos las nuevas dimensiones
            $newwidth = $width;
            $newheight = round($newwidth / $ratio);

            $img = imagecreatefromjpeg($file);
            
            // Creamos la miniatura
            $thumb = imagecreatetruecolor($newwidth, $newheight);
            // La redimensionamos
            imagecopyresampled($thumb, $img, 0, 0, 0, 0, $newwidth, $newheight, $file_info[0], $file_info[1]);
            // La mostramos como jpg
            $filename=$rutaMiniatura;
            @imagejpeg($thumb, $filename);
            imagedestroy($thumb);
        }
        
        public static function resizeImgBig($img,$rutaMiniatura){
            $file = $img;
            $width = 1024;

            // Ponemos el . antes del nombre del archivo porque estamos considerando que la ruta está a partir del archivo thumb.php
            $file_info = getimagesize($file);
            // Obtenemos la relación de aspecto
            $ratio = $file_info[0] / $file_info[1];

            // Calculamos las nuevas dimensiones
            $newwidth = $width;
            $newheight = round($newwidth / $ratio);

            $img = imagecreatefromjpeg($file);
            
            // Creamos la miniatura
            $thumb = imagecreatetruecolor($newwidth, $newheight);
            // La redimensionamos
            imagecopyresampled($thumb, $img, 0, 0, 0, 0, $newwidth, $newheight, $file_info[0], $file_info[1]);
            // La mostramos como jpg
            $filename=$rutaMiniatura;
            @imagejpeg($thumb, $filename);
            imagedestroy($thumb);
        }
        
        
}