<?php
class ComCustomButtonColumn extends CButtonColumn
{
	protected function renderButton($id, $button, $row, $data)
	{
		$button['imageUrl'] = Comentarios::getImagen($data->id);
		parent::renderButton($id, $button, $row, $data);
	}
}