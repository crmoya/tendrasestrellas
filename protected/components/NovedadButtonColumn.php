<?php
class NovedadButtonColumn extends CButtonColumn
{
    protected function renderButton($id, $button, $row, $data)
    {
        $button['imageUrl'] = Productos::getImagenNovedad($data->id);
        parent::renderButton($id, $button, $row, $data);
    }
}