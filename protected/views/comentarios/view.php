<?php
/* @var $this ComentariosController */
/* @var $model Comentarios */
?>

<h1>View Comentarios #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nombre',
		'comentario',
		'aceptado',
	),
)); ?>
