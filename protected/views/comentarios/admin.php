<?php
/* @var $this ComentariosController */
/* @var $model Comentarios */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#comentarios-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Comentarios</h1>

<?php echo CHtml::link('Búsqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'comentarios-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'nombre',
                'email',
		'comentario',
		array(
			'class'=>'ComCustomButtonColumn',
			'template'=>'{destacar}',
			'header'=>'Publicar / Despublicar',
			'buttons'=>array
			(
					'destacar' => array
					(
							'label'=>'Aceptar/Rechazar Comentario',
							'url'=>'Yii::app()->createUrl("comentarios/aceptar/$data->id")',
					),
			),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{delete}',
			'header'=>'Eliminar',
		),
	),
)); ?>
