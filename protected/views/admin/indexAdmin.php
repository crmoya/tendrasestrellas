<?php $this->pageTitle=Yii::app()->name; ?>
Bienvenido <?php echo CHtml::encode($nombre);?>, por favor seleccione una de las siguientes operaciones para comenzar:<br/><br/>

<div class="menu">
	
	<div class="boton">
		<?php 
		$imghtml=CHtml::image(Yii::app()->request->baseUrl.'/images/productos.jpg');
		echo CHtml::link($imghtml, CController::createUrl('//productos/admin')); 
		echo CHtml::link("Administrar Productos",CController::createUrl('//productos/admin'));
		?>
	</div>
	<div class="spacer"></div>
	<div class="boton">
		<?php 
		$imghtml=CHtml::image(Yii::app()->request->baseUrl.'/images/pedidos.jpg');
		echo CHtml::link($imghtml, CController::createUrl('//pedidos/admin')); 
		echo CHtml::link("Administrar Pedidos",CController::createUrl('//pedidos/admin'));
		?>
	</div>	
	<div class="spacer"></div>
	<div class="boton">
		<?php 
		$imghtml=CHtml::image(Yii::app()->request->baseUrl.'/images/categorias.jpg');
		echo CHtml::link($imghtml, CController::createUrl('//categorias/admin')); 
		echo CHtml::link("Administrar Categorías",CController::createUrl('//categorias/admin'));
		?>
	</div>
	<div class="spacer"></div>
	<div class="boton">
		<?php 
		$imghtml=CHtml::image(Yii::app()->request->baseUrl.'/images/regiones.jpg');
		echo CHtml::link($imghtml, CController::createUrl('//regiones/admin')); 
		echo CHtml::link("Administrar Regiones",CController::createUrl('//regiones/admin'));
		?>
	</div>
	<div class="spacer"></div>
	<div class="boton">
		<?php 
		$imghtml=CHtml::image(Yii::app()->request->baseUrl.'/images/ciudades.jpg');
		echo CHtml::link($imghtml, CController::createUrl('//comunas/admin')); 
		echo CHtml::link("Administrar Comunas",CController::createUrl('//comunas/admin'));
		?>
	</div>
        <br/><br/>
	<div class="boton">
		<?php 
		$imghtml=CHtml::image(Yii::app()->request->baseUrl.'/images/metodos_envios.jpg');
		echo CHtml::link($imghtml, CController::createUrl('//metodosEnvio/admin')); 
		echo CHtml::link("Administrar Métodos Envío",CController::createUrl('//metodosEnvio/admin'));
		?>
	</div>
	<div class="spacer"></div>
	<div class="boton">
                <br/>
		<?php 
		$imghtml=CHtml::image(Yii::app()->request->baseUrl.'/images/comentarios.jpg');
		echo CHtml::link($imghtml, CController::createUrl('//comentarios/admin')); 
		echo CHtml::link("<br/>Administrar Comentarios <br/> $comentarios",CController::createUrl('//comentarios/admin'));
		?>
	</div>
	<div class="spacer"></div>
	<div class="boton">
		<?php 
		$imghtml=CHtml::image(Yii::app()->request->baseUrl.'/images/usuarios.png');
		echo CHtml::link($imghtml, CController::createUrl('//usuarios/admin')); 
		echo CHtml::link("Administrar Usuarios",CController::createUrl('//usuarios/admin'));
		?>
	</div>
	<div class="spacer"></div>
        <div class="boton">
		<?php 
		$imghtml=CHtml::image(Yii::app()->request->baseUrl.'/images/usuarios.png');
		echo CHtml::link($imghtml, CController::createUrl('//clientes/admin')); 
		echo CHtml::link("Administrar Clientes",CController::createUrl('//clientes/admin'));
		?>
	</div>
	<div class="spacer"></div>
	<div class="boton">
		<?php 
		$imghtml=CHtml::image(Yii::app()->request->baseUrl.'/images/clave.jpg');
		echo CHtml::link($imghtml, CController::createUrl('cambiarClave')); 
		echo CHtml::link("Cambiar mi clave&nbsp;",CController::createUrl('cambiarClave'));
		?>
	</div><br/><br/>
	<div class="boton">
		<?php 
		$imghtml=CHtml::image(Yii::app()->request->baseUrl.'/images/camara.jpg');
		echo CHtml::link($imghtml, CController::createUrl('subirPortada')); 
		echo CHtml::link("Subir Foto Portada&nbsp;",CController::createUrl('subirPortada'));
		?>
	</div>
        <div class="spacer"></div>
	<div class="boton">
		<?php 
		$imghtml=CHtml::image(Yii::app()->request->baseUrl.'/images/camara.jpg');
		echo CHtml::link($imghtml, CController::createUrl('subirPortada2')); 
		echo CHtml::link("Subir Foto Portada 2&nbsp;",CController::createUrl('subirPortada2'));
		?>
	</div>
        <div class="spacer"></div>
        <div class="boton">
		<?php 
		$imghtml=CHtml::image(Yii::app()->request->baseUrl.'/images/camara.jpg');
		echo CHtml::link($imghtml, CController::createUrl('subirAbout')); 
		echo CHtml::link("Subir Foto About&nbsp;<br/><br/>",CController::createUrl('subirAbout'));
		?>
	</div>
        <div class="spacer"></div>
        <div class="boton">
		<?php 
		$imghtml=CHtml::image(Yii::app()->request->baseUrl.'/images/camara.jpg');
		echo CHtml::link($imghtml, CController::createUrl('subirCondiciones')); 
		echo CHtml::link("Subir Foto Condiciones&nbsp;",CController::createUrl('subirCondiciones'));
		?>
	</div>
	<div class="spacer"></div>
        <div class="boton">
		<?php 
		$imghtml=CHtml::image(Yii::app()->request->baseUrl.'/images/camara.jpg');
		echo CHtml::link($imghtml, CController::createUrl('subirFlyer')); 
		echo CHtml::link("Subir Flyer<br/><br/>",CController::createUrl('subirFlyer'));
		?>
	</div>
</div>