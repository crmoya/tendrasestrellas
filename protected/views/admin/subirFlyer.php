<div class="form">

<?php if(Yii::app()->user->hasFlash('flyerMessage')): ?>
<div class="flash-success">
<?php echo Yii::app()->user->getFlash('flyerMessage'); ?>
</div>
<?php endif; ?>
<?php if(Yii::app()->user->hasFlash('flyerError')): ?>
<div class="flash-error">
<?php echo Yii::app()->user->getFlash('flyerError'); ?>
</div>
<?php endif; ?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'portada-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'imagen'); ?>
		<?php echo $form->fileField($model,'imagen'); ?>
		<?php echo $form->error($model,'imagen'); ?>
	</div>
    
        <div class="row">
		<?php echo $form->labelEx($model,'show'); ?>
		<?php echo $form->checkbox($model,'show'); ?>
		<?php echo $form->error($model,'show'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Subir'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->