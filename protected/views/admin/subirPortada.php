<div class="form">

<?php if(Yii::app()->user->hasFlash('portadaMessage')): ?>
<div class="flash-success">
<?php echo Yii::app()->user->getFlash('portadaMessage'); ?>
</div>
<?php endif; ?>
<?php if(Yii::app()->user->hasFlash('portadaError')): ?>
<div class="flash-error">
<?php echo Yii::app()->user->getFlash('portadaError'); ?>
</div>
<?php endif; ?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'portada-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'imagen'); ?>
		<?php echo $form->fileField($model,'imagen'); ?>
		<?php echo $form->error($model,'imagen'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Subir'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->