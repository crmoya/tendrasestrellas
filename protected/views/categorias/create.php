<?php
/* @var $this CategoriasController */
/* @var $model Categorias */


$this->menu=array(
	array('label'=>'Listar Categorias', 'url'=>array('index')),
	array('label'=>'Administrar Categorias', 'url'=>array('admin')),
);
?>

<h1>Crear Categoría</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>