<?php
/* @var $this CategoriasController */
/* @var $model Categorias */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'categorias-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>
	
	<?php if(!$model->isNewRecord):?>
	<div class="row">
		<?php echo $form->labelEx($model,'producto_id'); ?>
		<?php 
			echo $form->dropDownList(
				$model,
				'producto_id',
				CHtml::listData(Productos::model()->findAllByAttributes(array('categorias_id'=>$model->id)), 'id', 'nombre'),
				array('prompt'=>'Seleccione un Producto',)
			);
		?>
		<?php echo $form->error($model,'categorias_id'); ?>
	</div>
	<?php endif;?>
	<!-- 
	<div class="row">
		<?php echo $form->labelEx($model,'imagen'); ?>
		<?php echo $form->fileField($model,'imagen'); ?>
		<?php echo $form->error($model,'imagen'); ?>
	</div>
 -->
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->