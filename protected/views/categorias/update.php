<?php
/* @var $this CategoriasController */
/* @var $model Categorias */

$this->menu=array(
	array('label'=>'List Categorias', 'url'=>array('index')),
	array('label'=>'Create Categorias', 'url'=>array('create')),
	array('label'=>'View Categorias', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Categorias', 'url'=>array('admin')),
);
?>

<h1>Editar Categorias <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<br/>
<!-- 
<div style="text-align:center;">
	<img src="<?php echo CController::createUrl("//site/imagen",array('ruta'=>"categorias/$model->id")); ?>"/>
</div>
 -->