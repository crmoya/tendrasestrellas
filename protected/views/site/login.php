<div class="form magenta">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row buttons">
            <div class="boton_comprar_chico login">Acceder</div>
            &nbsp;&nbsp;<a href="<?php echo CController::createUrl("//site/registrate");?>" class="link_path">Regístrate</a>
            <br/><br/>
            <a href="<?php echo CController::createUrl("//site/resetPassword");?>" class="link_path">¿Olvidaste tu clave? Haz click acá.</a>
	</div>

        
<?php $this->endWidget(); ?>
</div><!-- form -->
