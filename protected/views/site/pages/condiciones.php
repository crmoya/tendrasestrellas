<div class="papa_about">
    <div class="about no_clickeable">
        <img class="img_cond" src="<?php echo CController::createUrl("//site/imagenPublica",array('ruta'=>"portada/condiciones.jpg")); ?>"/>
    </div>
    &nbsp;&nbsp;
    <div class="about">
        Para comprar cualquier producto de Tendrás Estrellas tienes que hacer un pedido a través de esta página web; 
        las ventas se realizan únicamente por internet. No tengo una tienda física o lugar donde ver los productos; 
        despacho a todo Chile. 
        <br/><br/>
        Los modelos que aparecen en la página están disponibles, pero son hechos a pedido, por lo que una vez enviada una 
        orden, procedo a fabricar los productos solicitados y a entregarlos dentro de un plazo de 1 a 5 días dependiendo de la cantidad solicitada.
        En base a la demanda, los modelos pueden agotarse incluso durante el día, por lo que una vez recibido el pedido, confirmo el stock.
        <br/><br/>
        Todos los modelos son diseñados y fabricados por mí con materiales seleccionados cuidadosamente tanto en el país 
        como en el extranjero, asegurando su originalidad e innovación. Puedes estar segura de que no encontrarás otro igual.
        <br/><br/>
        Para acceder a los precios por mayor debes encargar una cantidad mínima de 15 productos combinados a elección. 
        Luego de un primer pedido por mayor, no es necesario volver a pedir esta cantidad en próximos pedidos.
        <br/><br/>
        Antes de enviar el pedido se confirma el pago en una cuenta bancaria que se entrega vía mail en respuesta al 
        pedido realizado por esta página web. Para el despacho es necesario llenar todos los campos en el formulario de registro:
        <ul style="text-align:left;">
            <li>Nombre y Apellido</li>
            <li>Rut</li>
            <li>Dirección de despacho (calle, número, comuna, ciudad, región)</li>
        </ul>
        <br/>
        También puedes retirar tu pedido en una sucursal de Correos o Chilexpress si prefieres.
        Si tienes una tienda o necesitas una colección especial personalizada, puedes comunicarme tus ideas con toda libertad.
        <span style="display:none;">bijoux bijouterie bijou artesanía hecho a mano complementos venta por mayor venta mayorista</span>
    </div>
</div>

