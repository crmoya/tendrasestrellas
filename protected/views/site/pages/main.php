<div class="telon" style="display:none;"></div>
<div class="sobre-telon" style="display:none;">
    <div class="flyer">
        <img class="close-btn" src="<?php echo Yii::app()->baseUrl;?>/images/close.png"/>
        <img class="img-flyer" src="<?php echo CController::createUrl("//site/imagenPublica",array('ruta'=>"flyer.jpg")); ?>"/>
    </div>
</div>
<script>
$(document).ready(function(e){
	
    var flyervisible = '<?php 
        $flyer = Flyer::model()->find();
        if($flyer != null){
            $show = $flyer->show;
            echo $show;
        }
    ?>';
    if(flyervisible == '0'){
        $('.telon').hide();
        $('.sobre-telon').hide();
    }
	else{
		$('.telon').show();
		$('.sobre-telon').show();
	}
    $(document).ready(function() {
        var tmpImg = new Image() ;
        tmpImg.src = $('.img-flyer').attr('src') ;
        $('.close-btn').hide();
        tmpImg.onload = function() {
            var ancho = $('.img-flyer').width();
            var pos = ($(window).width() - ancho)/2 + ancho - 15;
            var alto = $(window).height();
            $('.flyer').css('height',(alto-40)+"px");
            $('.close-btn').css('left',pos+"px");
            $('.close-btn').fadeIn();
        } ;
    }) ;
    $('.close-btn').click(function(e){
        $('.telon').fadeOut();
        $('.sobre-telon').fadeOut();
    });
    $('.img_novedad').click(function(e){
        window.location = '<?php echo CController::createUrl("//productos/");?>/'+$(this).attr('prod_id');
    });
});
</script>

<div class="center no_clickeable">
    <img src="<?php echo CController::createUrl("//site/imagenPublica",array('ruta'=>"portada/portada.jpg")); ?>"/>
</div>
<br/>
<div class="tit-novedad">NOVEDADES</div>
<br/>
<div class="novedades">
<?php foreach($novedades as $novedad){?>
    <div class="novedad">
        <img class="img_novedad no_clickeable" prod_id="<?php echo $novedad->productos_id;?>" src="<?php echo Yii::app()->baseUrl.'/images/productos/'.$novedad->productos_id.'/1.min'; ?>"/>
    </div>
<?php }?>
</div>
<br/>
<div class="row-fluid acerca_cuadro">
	<div class="span4 offset2 imagen_acerca">
            <img class="no_clickeable img_acerca" src="<?php echo CController::createUrl("//site/imagenPublica",array('ruta'=>"portada/portada2.jpg")); ?>"/>
	</div>
	<div class="span6 text portada_text">
		INSPIRACION<br/>
                La naturaleza en todo su esplendor, el brillo del ocaso que se 
                consume en un espéctaculo diario de arte y belleza, las nubes 
                reflejadas en un trozo de lago, las sombras que potencian el 
                nacimiento del color... sobran los motivos y falta espacio 
                para encapsularlos. Mi trabajo es un intento de hacerlo, 
                echando mano a la imaginación, procurando unir encanto y arte.
                <br/><br/>
                MATERIALES<br/>
                La fragilidad del cristal y el vidrio pintado a mano, la rusticidad 
                de la madera, la sutilidad de la organza, la naturaleza viva del fieltro, 
                la perdurabilidad de las piedras, la versatilidad del acrílico y la 
                transparencia de las filigranas, por nombrar sólo algunos...		
	</div>
</div>