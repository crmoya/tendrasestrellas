<div class="faq_izq">
    <ul>
        <li class="faq_titulo">¿Cómo creo mi cuenta?</li>
        <li class="faq_respuesta">
            ¡Es muy fácil! Haz click en “Mi cuenta” (extremo superior derecho) e ingresa tus datos y contraseña. 
            Una vez que tengas tu cuenta, podrás hacer pedidos, revisar tus datos y actualizar la información de ser necesario.
        </li>
        <li class="faq_titulo">¿Cómo hago mi pedido?</li>
        <li class="faq_respuesta">Cada producto tiene la opción "Añadir al Pedido". Puedes añadir todos los productos que desees, 
            los que se mostrarán en el menú superior de la página y, una vez añadidos todos los productos que quieras, se mostrará 
            una lista con el valor total. Si no te has registrado, una vez que hayas añadido todos los productos que quieras, 
            deberás crear una cuenta con tus datos personales para que puedas enviar el pedido y sea posible contactarte con los datos para el pago.</li>
        <li class="faq_titulo">¿Hay algún descuento si compro una cantidad determinada de productos?</li>
        <li class="faq_respuesta">
            Sí, hay un precio por mayor que aplica un descuento de alrededor del 50% en el total de tu pedido.
        </li>
        <li itemscope itemtype="http://schema.org/Product" class="faq_titulo">¿Hay un monto o cantidad mínima para acceder a los 
        <span itemprop="name">precios por mayor</span>?</li>
        <li class="faq_respuesta">
            El monto mínimo para acceder a los precios por mayor es de 15 productos. Estos pueden ser combinados a tu elección (ejemplo: 5 aros + 
            5 pulseras + 5 collares). Una vez que hayas llegado al monto mínimo, se aplicará el descuento por mayor automáticamente a tu pedido.
        </li>
        <li class="faq_titulo">¿Cuánto tiempo pasa desde que hago mi pedido hasta que lo recibo en mi casa?</li>
        <li class="faq_respuesta">
            Dependiendo de la cantidad de productos, el tiempo de demora para tener listo un pedido varía entre 2 y 4 días. Una vez listo, 
            el tiempo de despacho por Correos de Chile o Chilexpress demora entre 1 y 2 días hábiles dependiendo de la región.
        </li>
        <li class="faq_titulo">¿Quién paga el envío?</li>
        <li class="faq_respuesta">
            El envío lo paga el comprador. El precio del envío se suma al total del pedido una vez confirmado éste y la dirección de despacho.
        </li>
        <li class="faq_titulo">¿A qué cuenta debo efectuar el pago?</li>
        <li class="faq_respuesta">
            La cuenta para el pago será enviada a tu correo una vez que hayas enviado tu pedido. Sólo acepto depósitos en efectivo o 
            transferencias del monto total del pedido. Una vez confirmado el pago, se envía el pedido.
        </li>
        <li class="faq_titulo">¿Entregas factura?</li>
        <li class="faq_respuesta">
            Muy pronto...
        </li>
        <li class="faq_titulo">¿De qué materiales son los productos?</li>
        <li class="faq_respuesta">
            Todas las creaciones son hechas de 4 tipos de metales: alpaca, plata tibetana, bronce, y cobre. 
            <span itemscope itemtype="http://schema.org/Product">
<span itemprop="name">bisutería</span>
            </span>confeccionada a mano 
<span itemscope itemtype="http://schema.org/Product">
<span itemprop="name">artesanalmente</span></span>. Algunas de las cuentas ocupadas en los diseños son: 
<span itemscope itemtype="http://schema.org/Product">
<span itemprop="name">vidrios</span></span>, 
<span itemscope itemtype="http://schema.org/Product">
<span itemprop="name">cristales</span> </span>de roca, porcelana, madera, 
            arcilla polimérica, resina, acrílico, 
<span itemscope itemtype="http://schema.org/Product">
<span itemprop="name">piedras</span> </span>naturales, 
<span itemscope itemtype="http://schema.org/Product">
<span itemprop="name">perlas</span> </span>de río, piedra 
<span itemscope itemtype="http://schema.org/Product">
<span itemprop="name">howlita</span></span>, 
<span itemscope itemtype="http://schema.org/Product">
<span itemprop="name">filigranas</span></span>, cuentas esmaltadas (
<span itemscope itemtype="http://schema.org/Product">
<span itemprop="name">cloisonne</span></span>), 
            
<span itemscope itemtype="http://schema.org/Product">
<span itemprop="name">rhinestones</span></span>, organza, 
<span itemscope itemtype="http://schema.org/Product">
<span itemprop="name">gamuza</span></span>, flores de papel, seda, tejidos en 
<span itemscope itemtype="http://schema.org/Product">
<span itemprop="name">macramé</span> </span>y 
<span itemscope itemtype="http://schema.org/Product">
<span itemprop="name">fieltro</span> </span>agujado.
        </li>
        <li class="faq_titulo">¿Cómo recibo mi envío?</li>
        <li class="faq_respuesta">
            Cada producto viene en una bolsita de celofán transparente con un sticker. El pedido te llegará en una caja bien protegida para que tus 
            productos lleguen en óptimas condiciones.
        </li>
        <li class="faq_titulo">¿Tienen garantía los productos?</li>
        <li class="faq_respuesta">
            Si tu artículo se rompiese a causa de una elaboración defectuosa en las primeras 2 semanas de la fecha de compra, puedes enviarme una foto 
            y te enviaré un producto igual u otro del mismo valor. Si quisieras cambiar un producto por alguna otra razón, puedes enviármelo de vuelta 
            pagando tú el envío.
        </li>
        <li class="faq_titulo">Si no encuentro el modelo exacto que quiero, ¿Puedo mandar a hacerlo?</li>
        <li class="faq_respuesta">
            ¡Por supuesto! Si necesitas un modelo en otro color o tienes una idea en mente, puedes contármela. También he creado 
            <span itemscope itemtype="http://schema.org/Product">
<span itemprop="name">colecciones exclusivas</span> </span>para tiendas o para ocasiones especiales, como fiestas o 
<span itemscope itemtype="http://schema.org/Product">
<span itemprop="name">matrimonios</span></span>. Puedes comunicarte conmigo a 
            <strong><a class="link_cuenta" href="mailto:contacto@tendrasestrellas.cl">contacto@tendrasestrellas.cl</a></strong>
        </li>
    </ul>
</div>
<div class="faq_der">
    Si tienes otra pregunta, por favor escríbeme a <strong><a class="link_cuenta" href="mailto:contacto@tendrasestrellas.cl">contacto@tendrasestrellas.cl</a></strong>
</div>
