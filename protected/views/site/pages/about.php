<div class="papa_about">
    <div class="about no_clickeable">
        <img class="img_cond" src="<?php echo CController::createUrl("//site/imagenPublica",array('ruta'=>"portada/about.jpg")); ?>"/>
    </div>
    &nbsp;&nbsp;
    <div class="about">
        Mi nombre es María José, soy licenciada en literatura, me apasiona la idea de expresar algo de mí con las palabras, 
        pero me di cuenta de que también podía hacerlo con las manos... por eso empecé este trabajo de diseñadora y artesana 
        que comenzó como un hobby y hoy es parte de mí tanto como lo son las letras.<br/><br/>
        
        Siempre me han atraído la joyería artesanal, los accesorios, los aros gigantes, los colores, los brillos en general. 
        Quiero compartir este gusto con quienes disfrutan de tener algo especial y hecho a mano. 
        Tendrás Estrellas es un mensaje para aquellas que quieren brillar de manera diferente 
        y que saben que es posible tener estrellas que incluso sepan reír, como diría un principito.<br/><br/>
        
        Trabajo sola en mi taller en casa y cada uno de los diseños exclusivos que elaboro salen de mi imaginación, de la exploración de 
        tendencias, de los sueños que tengo, de los destellos diarios que el sol me provee. Me preocupa mucho la exclusividad, 
        en un mundo en que muchos de los accesorios se repiten y cuesta encontrar el complemento original que calce perfecto 
        con la personalidad. Es por esto que busco cuidadosamente materiales que sirvan para contar historias únicas. 
        De las técnicas más delicadas, descubrí el fieltro, y la capacidad de crear figuras de un trozo de vellón me asombra 
        cada vez más. Las posibilidades de colores son infinitas y el resultado es tan sutil que los diseños parecen volar de 
        lo livianos que son.<br/><br/> 
        
        Los invito a recorrer y a tener estrellas.
    </div>
</div>