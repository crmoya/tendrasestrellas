<p class="pedido_titulo">Mi pedido Actual</p>
<?php if(Yii::app()->user->hasFlash('errorPedido')): ?>
<div class="flash-error">
    <?php echo Yii::app()->user->getFlash('errorPedido'); ?>
</div>
<?php endif; ?>
<br/>
<?php 
    $carro = new Carro();
    if(isset(Yii::app()->session['carro'])){
        $carro = Yii::app()->session['carro'];
    }
    $items = $carro->items();
    $num_productos = $carro->num_productos();
    $total = 0;
    $label_precio ="Precio";
    if($num_productos >= Tools::por_mayor){
        $label_precio = "Precio por Mayor";
    }
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pedido-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
<table class="cabecera_tabla">
    <tr>
        <th><div class="align_left">Producto</div></th>
        <th><?php echo $label_precio;?></th>
        <th>Cantidad</th>
        <th>Subtotal</th>
        <th></th>
    </tr>
    <?php
    foreach($items as $item){
        $producto = Productos::model()->findByPk($item->producto_id);
        $nombre = $producto->nombre;
        $precio = (int)$producto->precio;
        $prod_id = $item->producto_id;
        if($num_productos >= Tools::por_mayor){
            $precio = (int)$producto->precio_por_mayor;
        }
        $cantidad = (int)$item->cantidad;
        $subtotal = (int)$cantidad*(int)$precio;
        $total += $subtotal;
        echo "<tr class='line'>";
        echo "  <td><div class='align_left'><a href='".CController::createUrl("//productos/".$producto->id)."'><img class='img_chica' src='".Yii::app()->baseUrl."/images/productos/".$producto->id."/1.min'/></a>&nbsp;<span class='inline_block'>$nombre</span></div></td>"
            . " <td>".Tools::formateaPlata($precio)."</td>"
            . " <td><input type='number' value='".$cantidad."' name='cantidad' min='1' prod_id='".$producto->id."' max='99' class='numeric fixed'/></td>"
            . " <td>".Tools::formateaPlata($subtotal)."</td>"
            . " <td><img class='eliminar_pedido' prod_id='".$prod_id."' src='".Yii::app()->baseUrl."/images/eliminar.png'/></td>";
        echo "</tr>";
    }
    
    $usuario = Usuarios::model()->findByPk(Yii::app()->user->id);
    if($usuario != null){
        $cliente = Clientes::model()->findByAttributes(array('usuarios_id'=>$usuario->id));
        $metodo_envio_id = 1;
        $comuna = Comunas::model()->findByPk($cliente->comunas_id);
        $region = $comuna->regiones;
        $costo_envio = CostosEnvio::model()->findByAttributes(array('regiones_id'=>$region->id,'metodos_envio_id'=>$metodo_envio_id));
        $envio = $costo_envio->costo;
        if($total >= Tools::despacho_gratis){
            $envio = 0;
        }
    }else{
        $envio = 0;
    }
    ?>
    <tr class="no_line"><td></td><td>Costo del Pedido:</td><td></td><td class="costo_pedido"><input type="hidden" name="costo_pedido" value="<?php echo $total;?>"/><?php echo Tools::formateaPlata($total);?></td></tr>
    <tr class="no_line"><td></td><td><input type="hidden" name="costo_envio" value=""/>
        Método de Envío:
        </td><td><?php
        
        echo $form->dropDownList(
                $model,
                'metodo_envio',
                CHtml::listData(MetodosEnvio::model()->findAll(), 'id', 'nombre'),
                array('prompt'=>'Seleccione un Método de Envío',)
        );
    ?><?php echo $form->error($model,'metodo_envio'); ?></td>
        <td id="costo_envio"></td></tr>
    <tr class="no_line"><td></td><td>Costo Total:</td><td><input type="hidden" name="costo_total" value=""/></td><td id="costo_total"></td></tr>
</table>
<?php $this->endWidget(); ?>
<div class="row center">
    <div class="boton_comprar_chico btn_aceptar">Enviar Pedido</div>
</div>
<script>
$(document).ready(function(e){
    
    function formatThousands(value, separator) {
        var buf = [];
        value = String(value).split('').reverse();
        for (var i = 0; i < value.length; i++) {
            if (i % 3 === 0 && i !== 0) {
                buf.push(separator);
            }   
            buf.push(value[i]);
        }
        return buf.reverse().join('');
    }
    
    $('.btn_aceptar').click(function(e){
        if(confirm('¿Estás seguro/a de que quieres enviar este pedido?')){
            $("#pedido-form").submit();
        }
        else{
        }
        
    });
    
    
    function cambiaEnvio(metodo){
        <?php 
        if(!Yii::app()->user->isGuest){
            $cliente = Clientes::model()->findByAttributes(array('usuarios_id'=>$usuario->id));
            $comuna = Comunas::model()->findByPk($cliente->comunas_id);
            $region = $comuna->regiones;
        ?>
        var region = '<?php echo $region->id?>';
        var total = '<?php echo $total;?>';
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createUrl('//site/actualizarMetodo/');?>",
            data: { metodo: metodo,region:region,total:total}
        }).done(function( msg ) {
            var costo = msg;
            $('#costo_envio').html("$ "+formatThousands(costo,'.'));
            
            $('input[name="costo_envio"]').val(costo);
            var costo_pedido = $('input[name="costo_pedido"]').val();
            var costo_total = Number(costo) + Number(costo_pedido);
            $('#costo_total').html("$ "+formatThousands(costo_total,'.'));
            
            $('input[name="costo_total"]').val(costo_total);
        });
        <?php
        }
        ?>
       
    }
    
    cambiaEnvio($("#PedidoForm_metodo_envio").val());
    
    $('#PedidoForm_metodo_envio').change(function(e){
        var metodo = $(this).val();
        cambiaEnvio(metodo);
    });

    $('.numeric').change(function(e){
        var prod_id = $(this).attr('prod_id');
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createUrl('//site/actualizarPedido/');?>",
            data: { prod_id: prod_id, cantidad: $(this).val()}
        }).done(function( msg ) {
            var agregados = msg;
            if(agregados > 0){
                $(".cant_pedido").html('('+agregados+')');
            }
            window.location = '<?php echo Yii::app()->createUrl('//site/pedido/');?>';
        });
        
    });
    
    $('.eliminar_pedido').click(function(e){
        var prod_id = $(this).attr('prod_id');
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createUrl('//site/eliminarPedido/');?>",
            data: { prod_id: prod_id}
        }).done(function( msg ) {
            var agregados = msg;
            if(agregados > 0){
                $(".cant_pedido").html('('+agregados+')');
            }
            window.location = '<?php echo Yii::app()->createUrl('//site/pedido/');?>';
        });
        
    });
});
</script>