<div class="form magenta">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

<?php
if(Yii::app()->user->hasFlash('error')):
?>
    <div class="alert-error">
        <?php echo Yii::app()->user->getFlash('error');?>
    </div>    
<?php   
endif;
if(Yii::app()->user->hasFlash('ok')):
?>
    <div class="alert-success">
        <?php echo Yii::app()->user->getFlash('ok');?>
    </div>    
<?php   
endif;
?><br/>
    <p>Ingresa tu email y pincha "Recuperar mi Clave". <br/>
        Te enviaremos un correo con las instrucciones
        necesarias para reestablecer tu clave</p>
	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row buttons">
            <div class="boton_comprar_chico login">Recuperar mi Clave</div>
	</div>

        
<?php $this->endWidget(); ?>
</div><!-- form -->
