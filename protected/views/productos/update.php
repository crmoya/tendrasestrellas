<?php
/* @var $this ProductosController */
/* @var $model Productos */


$this->menu=array(
	array('label'=>'List Productos', 'url'=>array('index')),
	array('label'=>'Create Productos', 'url'=>array('create')),
	array('label'=>'View Productos', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Productos', 'url'=>array('admin')),
);
?>

<h1>Actualizar Productos <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>