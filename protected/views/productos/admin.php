<?php 
$cs=Yii::app()->clientScript;
$cs->registerCoreScript('jquery'); 
?>
<script type="text/javascript">
	$(function() {

		
				
	});
</script>
<?php
/* @var $this ProductosController */
/* @var $model Productos */



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#productos-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php if(Yii::app()->user->hasFlash('productosError')): ?>
<div class="flash-error">
<?php echo Yii::app()->user->getFlash('productosError'); ?>
</div>
<?php endif; 



?>

<h1>Administrar Productos</h1>

<?php echo CHtml::link('Búsqueda Avanzada','#',array('class'=>'search-button')); ?>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?php echo CHtml::link('Crear Producto',CController::createUrl('//productos/create')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'productos-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'nombre',
		array('name'=>'categoria','value'=>'$data->categorias->nombre'),
		'precio',
		'descripcion',
		
		'precio_por_mayor',
                array(  'name'=>'disponible','value'=>'Tools::disponible($data->disponible)',
                        'filter' => CHtml::listData( array(
                                                        array('id'=>'1','nombre'=>'TODOS'),
                                                    ), 'id', 'nombre'),),
		array(
                    'class'=>'CustomButtonColumn',
                    'template'=>'{disponible}',
                    'header'=>'Disponible',
                    'buttons'=>array
                    (
                        'disponible' => array
                        (
                            'label'=>'Cambiar disponibilidad',
                            'url'=>'Yii::app()->createUrl("productos/disponibilidad/$data->id")',
                        ),
                    ),
		),
                array(
                    'class'=>'NovedadButtonColumn',
                    'template'=>'{novedad}',
                    'header'=>'Novedad',
                    'buttons'=>array
                    (
                        'novedad' => array
                        (
                            'label'=>'Cambiar novedad',
                            'url'=>'Yii::app()->createUrl("productos/novedad/$data->id")',
                        ),
                    ),
		),
		array(
                    'class'=>'CButtonColumn',
		),
	),
)); ?>
