<a class="link_path" href="<?php echo CController::createUrl("//");?>">
	<i>inicio</i>
</a>
>
<?php 
        $categoria = Categorias::model()->findByPk($model->categorias_id);
	if($categoria!=null):
?>
		<a class="link_path" href="<?php echo CController::createUrl("//categoria/".$categoria->id);?>">
			<i><?php echo CHtml::encode($categoria->nombre);?></i>
		</a>
<?php 
	endif;
        echo "> ";
	echo "<span class='text'>".CHtml::encode($model->nombre)."</span>";
?>
<?php
$user_id = Yii::app()->user->id;
$user = Usuarios::model()->model()->findByPk($user_id);
if($user != null){
$rol = $user->rol;
if($rol == 'administrador'):
?>
<div class="div_ancho"><?php echo CHtml::link("Crear otro producto",CController::createUrl("//productos/create"));?></div>
<?php
endif;
}
?>
<br/><br/>
<div class="mostrador_producto">
    <span class="imagen_mostrador no_clickeable">
        <?php $imagenes = $model->getImagenes();?>
        <div class="ppy" id="ppy1">
            <ul class="ppy-imglist">
            <?php  foreach($imagenes as $imagen):  ?>                
                <li>
                    <a href="<?php echo CController::createUrl("//site/imagenPublica",array('ruta'=>"productos/$model->id/".$imagen)); ?>">
                        <img src="<?php echo Yii::app()->baseUrl.'/images/productos/'.$model->id.'/'.$imagen.'.min';; ?>" alt="" />
                    </a>
                    <span class="ppy-extcaption">
                        <strong><?php echo $model->nombre; ?></strong>
                    </span>
                </li>
            <?php endforeach;?>
            </ul>
            <div class="ppy-outer">
                <div class="ppy-stage">
                    <div class="ppy-nav">
                        <a class="ppy-prev" title="Imagen anterior">Imagen anterior</a>
                        <a class="ppy-switch-enlarge" title="Zoom">Zoom</a>
                        <a class="ppy-switch-compact" title="Cerrar">Cerrar</a>
                        <a class="ppy-next" title="Próxima imagen">Próxima imagen</a>
                    </div>
                </div>
            </div>
            <div class="ppy-caption">
                <div class="ppy-counter">
                    Imagen <strong class="ppy-current"></strong> de <strong class="ppy-total"></strong> 
                </div>
                <span class="ppy-text"></span>
            </div>
        </div>
      
    </span>
    <div class="descripcion_mostrador">
        <p class="titulo-prod"><?php echo CHtml::encode($model->nombre);?></p>
        <br/>
        <p class="desc_prod">
            <?php echo CHtml::encode($model->descripcion);?>
        </p>
        <?php if($model->disponible == 0): ?>
        <p class="titulo-prod-nodisp">
            NO DISPONIBLE
        </p>
        <br/>
        <?php endif; ?>
        <div id="no_disponible" style="display:none;"><?php echo $model->disponible?></div>
        <p class="precio_prod">
            <?php echo Tools::formateaPlata($model->precio);?>
        </p>
        <br/>
        <p class="precio_mayor">
            <?php echo Tools::formateaPlata($model->precio_por_mayor)." por mayor";?>
        </p>
        <br/>
        <div>
            <span class="box_comprar">
                <!--Cantidad:
                <input maxlength="4" type="text" class="cantidad_text fixed" value="1"/>-->
                <div class="boton_comprar"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/carroBlanco.png"/>&nbsp;Añadir al Pedido</div>
            </span>
        </div>
        <br/>
        <div class="nav_prod">
            <span class="nav_prod_l">
                <?php $prev_id = $model->previousId(); 
                if($prev_id > -1):?>
                <a href="<?php echo $prev_id; ?>" class="link_path"><- anterior</a>
                <?php endif;?>
            </span>
            <span class="nav_prod_r">
                <?php $next_id = $model->nextId(); 
                if($next_id > -1):?>
                <a href="<?php echo $next_id;  ?>" class="link_path">siguiente -></a>
                <?php endif;?>
            </span>
        </div>
    </div>
</div>

<script>
$(document).ready(function(e){
    var options = {
        opacity: 1,
        easing: 'linear',
    };
    
    $('#ppy1').popeye(options);
    $('#ppy1').click(function(){ //Id del elemento cliqueable
        $('html, body').animate({scrollTop:280}, 'slow');
        return false;
    });
    
    $(".imagen_chica").click(function(e){
        var imagen_id = $(this).attr("imagen_id");
        var src = $("#imagen_grande").attr("src");
        try{
            var cortado = src.substring(0,src.lastIndexOf("%2F"));
            $("#imagen_grande").attr("src",cortado+"%2F"+imagen_id);
        }catch(exp){}
    });
    
    $(".boton_comprar").click(function(e){
        var disponible = $('#no_disponible').html();
        if(disponible.trim() == '1'){
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->createUrl('//site/agregarPedido/');?>",
                data: { prod_id: <?php echo $model->id; ?>, cantidad: $(".cantidad_text").val()}
            }).done(function( msg ) {
                var agregados = msg;
                if(agregados > 0){
                    $(".cant_pedido").html('('+agregados+')');
                }
                window.location = '<?php echo Yii::app()->createUrl('//site/pedido/');?>';
            });
        }
        else{
            alert('No puede agregar este producto al pedido, pues no está disponible por el momento.');
        }
        
    });
    
});
</script>