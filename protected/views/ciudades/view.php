<?php
/* @var $this CiudadesController */
/* @var $model Ciudades */


$this->menu=array(
	array('label'=>'Listar Ciudades', 'url'=>array('index')),
	array('label'=>'Crear Ciudades', 'url'=>array('create')),
	array('label'=>'Editar Ciudades', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Eliminar Ciudad', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Seguro desea eliminar esta ciudad?')),
	array('label'=>'Administrar Ciudades', 'url'=>array('admin')),
);
?>

<h1>Ver Ciudades #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nombre',
		array('name'=>'region','value'=>$model->regiones->nombre),
	),
)); ?>
