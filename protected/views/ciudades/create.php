<?php
/* @var $this CiudadesController */
/* @var $model Ciudades */

$this->menu=array(
	array('label'=>'List Ciudades', 'url'=>array('index')),
	array('label'=>'Manage Ciudades', 'url'=>array('admin')),
);
?>

<h1>Crear Ciudad</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>