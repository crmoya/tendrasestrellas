<?php
/* @var $this CiudadesController */
/* @var $dataProvider CActiveDataProvider */
?>

<h1>Ciudades</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
