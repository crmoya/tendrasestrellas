<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<!-- Marcado de microdatos añadido por el Asistente para el marcado de datos estructurados de Google. -->
<html>
<head>
	<meta charset="utf-8" />
	<meta name="language" content="es" />
	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.ico" rel="icon" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<title><?php echo CHtml::encode(Yii::app()->name); ?></title>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.ba-bbq.min.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/script.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.format.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/popeye/jquery.popeye.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/popeye/jquery.popeye.style.css" media="screen" />
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.popeye-2.1.js"></script>
</head>

<body>
<div id="fb-root"></div>
<script>
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<?php if(Yii::app()->user->isGuest || Yii::app()->user->rol == 'cliente'):?>
<div id="cabecera">
	<div id="el_menu" class="container-fluid">
		<div class="row-fluid menu_ppal">
                        <?php if(!Yii::app()->user->isGuest){echo CHtml::link('Salir ('.Yii::app()->user->nombre.')',array('//site/logout'),array('class'=>'pull-right btn-link btn maschico'));} ?>
                        <a class="pull-right btn-link btn maschico" href="<?php echo CController::createUrl('//site/pedido');?>">MI PEDIDO <div class="cant_pedido"></div></a>
			<?php echo CHtml::link('MI CUENTA',array('//site/cuenta'),array('class'=>'middle pull-right btn-link btn')); ?>
			<?php echo CHtml::link('CONDICIONES DE COMPRA',array('//site/condiciones'),array('class'=>'pull-right btn-link btn')); ?>
			<?php echo CHtml::link('SOBRE MÍ',array('//site/quienes'),array('class'=>'pull-right btn-link btn')); ?>
			<?php echo CHtml::link('PREGUNTAS FRECUENTES',array('//site/frecuentes'),array('class'=>'pull-right btn-link btn')); ?>
                        <a class="facebook" href="http://www.facebook.com/tendrasestrellas"><img src="<?php echo Yii::app()->baseUrl;?>/images/facebook.png"/></a>
                        <a class="pinterest" href="http://www.pinterest.com/tendrasestrella/"><img src="<?php echo Yii::app()->baseUrl;?>/images/pinterest.png"/></a>
                        <a class="instagram" href="https://www.instagram.com/tendrasestrellas_accesorios/"><img src="<?php echo Yii::app()->baseUrl;?>/images/instagram.png"/></a>
                        <div class="fb-like" data-href="<?php echo Tools::FaceBook;?>" data-width="70" data-layout="button_count" data-share="false" data-colorscheme="light" data-show-faces="true" data-send="true"></div>			   
		</div>
	</div>
</div>
<div class="texto_promo">DESPACHO <span class="color">GRATIS</span> POR COMPRAS DESDE $35.000</div>
<div class="container-fluid">
	<div class="espaciosuperior"></div>
	<div class="center">
		<div class="imagenLogo no_clickeable">
			<a href="<?php echo Yii::app()->request->baseUrl;?>/index.php"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png"/></a>
		</div>
		<div class="menucategorias">
			<?php 
			$categorias = Categorias::model()->findAll();
			$cuantas = count($categorias);
			for($i=0;$i<$cuantas-1;$i++):
			?>
			<div itemscope itemtype="http://schema.org/Product" class="boton_fila categoria" cat_id='<?php echo $categorias[$i]->id;?>'>
				<div itemprop="name" class="inner_cat">
					<?php echo $categorias[$i]->nombre;?>
				</div>
			</div>
			<?php
			endfor;
			?>
			<div class="boton_fila categoria" cat_id='<?php if(isset($categorias[$i])) echo $categorias[$i]->id;?>'>
				<?php if(isset($categorias[$cuantas-1])) echo $categorias[$cuantas-1]->nombre;?>
			</div>
		</div>
	</div>
<?php endif;?>
<?php if(!Yii::app()->user->isGuest && Yii::app()->user->rol != 'cliente'):?>
	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>array('admin/index'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/admin/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
	</div>	
<?php endif;?>
<?php echo $content; ?>
<div class="clear"></div>
</div><!-- page -->
<?php if(Yii::app()->user->isGuest || Yii::app()->user->rol == 'cliente'):?>
<div id="footer">
		<div class="menu-abajo">
			SÍGUEME
			<div class="siguenos">
                            <span class="span-3"><a href="http://www.facebook.com/tendrasestrellas">facebook</a></span>
                            <span><a href="http://www.facebook.com/tendrasestrellas"><img src="<?php echo Yii::app()->baseUrl;?>/images/facebook.png"/></a></span><br/>
                            <span class="span-3"><a href="http://www.pinterest.com/tendrasestrella" class="external_link">pinterest</a></span>
                            <span><a href="http://www.pinterest.com/tendrasestrella"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/pinterest.png"/></a></span><br/>
                            <span class="span-3"><a href="https://www.instagram.com/tendrasestrellas_accesorios/" class="external_link">instagram</a></span>
                            <span><a href="https://www.instagram.com/tendrasestrellas_accesorios/"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/instagram.png"/></a></span>
                            
			</div>
                        <br/>
                        <div class="precios_mayor">
                            ¿Quieres información sobre precios por mayor? <br/><a class="link" href="<?php echo CController::createUrl('//site/frecuentes');?>">Haz click aquí.</a>
			</div>
		</div>
		<div class="menu-abajo">
			DÉJAME TUS COMENTARIOS
			<br/><br/>
			<div id="comentarios">
				<div>
				<?php 
				$model = new Comentarios();
				$form=$this->beginWidget('CActiveForm', array(
					'id'=>'comentarios-form',
					'enableAjaxValidation'=>false,
				)); ?>
					<div>
						<span class="chica">Nombre: </span><br/>
						<?php echo $form->textField($model,'nombre',array('size'=>100,'maxlength'=>100,'id'=>'name')); ?>
					</div>
                                        <div>
						<span class="chica">Email:</span><br/>
						<?php echo $form->textField($model,"email",
										array(	'size'=>100,'maxlength'=>100,'id'=>'email')); ?>
					</div>
					<div>
						<span class="chica">Comentarios:</span><br/>
						<?php echo $form->textArea($model,"comentario",
										array(	'class'=>'upper','cols'=>'100','rows'=>'2','id'=>'comment',
												'style'=>'overflow:auto;resize:none;color:#B300B2;')); ?>
					</div>
				
					<div>
						&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;<?php echo CHtml::submitButton('Enviar',array('class'=>'btn','style'=>'color:#B300B2;font-size:8pt','id'=>'enviar-btn')); ?>
					</div>				
				<?php $this->endWidget(); ?>				
				</div>
			</div>
		</div>
		<div class="menu-abajo">
		LO QUE OTROS DICEN
		<br/><br/>
		<marquee class="otros_dicen" behavior="scroll" direction="up" scrollamount="1">
		<?php 
		$comentarios = Comentarios::model()->findAllByAttributes(array('aceptado'=>1),array('order'=>'id DESC'));
		foreach($comentarios as $comentario){
			echo "<b>".$comentario->nombre."</b>: ".$comentario->comentario."<br/><br/>";
		}	
		?>
		</marquee>
	</div>
</div><!-- footer -->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>                        
<?php endif;?>
<!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
    <script>
    $(document).ready(function(e){
        
        $('.fb-like').css('top',$(window).height()-25);
    	$(window).keydown(function(event){
    	    if(event.keyCode == 13) {
    	      event.preventDefault();
    	      return false;
    	    }
    	});
    	$("#enviar-btn").click(function(e){
        	var nombre = $('#name').val();
        	var comentario = $('#comment').val();
                var email = $('#email').val();
                if(nombre != '' && comentario != '' && email != ''){
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Yii::app()->createUrl('//site/addComment/');?>",
                        data: { nombre: nombre,comentario: comentario, email:email }
                      }).done(function( msg ) {
                              $('#name').val('');
                              $('#comment').val('');
                              $('#email').val('');
                              alert('Gracias por tu comentario.')
                      });
                }
                else{
                    alert('Debes ingresar tu nombre, email y comentarios para enviarnos un mensaje.');
                }
    		
  			return false;
        });

    	$('.categoria').click(function(e){
            window.location = '<?php echo CController::createUrl("//categoria/");?>/'+$(this).attr('cat_id');
        });
                               
        var cantidad = '<?php 
                            if(isset(Yii::app()->session['carro'])){
                                $carro = Yii::app()->session['carro'];
                                $total = $carro->num_productos();
                                echo $total;
                            }
                            else
                                echo 0;
                        ?>';
        if(cantidad > 0){
            $('.cant_pedido').html('('+cantidad+')');
        }
    });
    </script>
</body>
</html>