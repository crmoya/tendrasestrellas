<?php
/* @var $this CiudadesController */
/* @var $model Ciudades */


$this->menu=array(
	array('label'=>'Listar Comunas', 'url'=>array('index')),
	array('label'=>'Crear Comunas', 'url'=>array('create')),
	array('label'=>'Editar Comuna', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Eliminar Comuna', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Seguro desea eliminar esta comuna?')),
	array('label'=>'Administrar Comunas', 'url'=>array('admin')),
);
?>

<h1>Ver Comuna #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nombre',
		array('name'=>'ciudad','value'=>$model->ciudades->nombre),
	),
)); ?>
