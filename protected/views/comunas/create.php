<?php
/* @var $this CiudadesController */
/* @var $model Ciudades */

$this->menu=array(
	array('label'=>'List Comunas', 'url'=>array('index')),
	array('label'=>'Manage Comunas', 'url'=>array('admin')),
);
?>

<h1>Crear Comuna</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>