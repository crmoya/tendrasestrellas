<?php
/* @var $this CiudadesController */
/* @var $model Ciudades */

$this->menu=array(
	array('label'=>'List Comunas', 'url'=>array('index')),
	array('label'=>'Create Comunas', 'url'=>array('create')),
	array('label'=>'View Comunas', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Comunas', 'url'=>array('admin')),
);
?>

<h1>Editar Comuna <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>