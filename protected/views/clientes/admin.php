<?php
/* @var $this ClientesController */
/* @var $model Clientes */

$this->menu=array(
	array('label'=>'Listar Clientes', 'url'=>array('index')),
	array('label'=>'Crear Clientes', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#clientes-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Clientes</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'clientes-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
            'rut',
            array('name'=>'user','value'=>'$data->usuarios->user'),
            array('name'=>'nombre','value'=>'$data->usuarios->nombre'),
            'direccion',
            array('name'=>'comuna','value'=>'$data->comunas->nombre'),
            array('name'=>'region','value'=>'$data->comunas->regiones->nombre'),
	),
)); ?>
