<?php
if(Yii::app()->user->hasFlash('error')):
?>
    <div class="alert-error">
        <?php echo Yii::app()->user->getFlash('error');?>
    </div>    
<?php   
endif;
if(Yii::app()->user->hasFlash('ok')):
?>
    <div class="alert-success">
        <?php echo Yii::app()->user->getFlash('ok');?>
    </div>    
<?php   
endif;
?>

<div class="form_cambio">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Campos con <span class="required">*</span> son requeridos.</p>

	<div class="row">
		<?php echo $form->labelEx($model,'nueva'); ?>
		<?php echo $form->passwordField($model,'nueva'); ?>
		<?php echo $form->error($model,'nueva'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'repita'); ?>
		<?php echo $form->passwordField($model,'repita'); ?>
		<?php echo $form->error($model,'repita'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Cambiar Clave',array('class'=>'btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
