<div class="form magenta mag_center">
<?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
            'validateOnSubmit'=>true,
	),
)); ?>
    <script>
    $(document).ready(function(e){
        
        $("#RegistrateForm_region_id").change(function(e){
            $("#comuna_id").html("<option value=''>Seleccione una Comuna</option>");
        });
        
        $("#comuna_id").html("<option value='<?php echo $model->comuna_id?>'><?php echo $comuna_nombre?></option>");
                
        $('.btn_aceptar').click(function(e){
            $('#login-form').submit();
            //e.preventDefault();
        });
    });
    </script>
    <br/>
<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>
<?php if(Yii::app()->user->hasFlash('error')):?>
    <div class="flash-error">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
<?php endif; ?>
    <div class="mag_left">
        <p class="titulo_reg">Actualiza tus datos</p>
        <div class="cuadro">
            <div class="row">
                <?php echo $form->labelEx($model,'username'); ?>
                <?php echo $form->textField($model,'username',array('placeholder'=>'correo@correo.com','readonly'=>'readonly')); ?>
                <?php echo $form->error($model,'username'); ?><div class="error_usuario"></div>
            </div>
            
            <div class="row">
                <?php echo $form->labelEx($model,'rut'); ?>
                <?php echo $form->textField($model,'rut',array('placeholder'=>'12345678-9')); ?>
                <?php echo $form->error($model,'rut'); ?><div class="error_usuario"></div>
            </div>
            
            <div class="row">
                <?php echo $form->labelEx($model,'nombre'); ?>
                <?php echo $form->textField($model,'nombre',array('placeholder'=>'Nombre Apellido')); ?>
                <?php echo $form->error($model,'nombre'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'direccion'); ?>
                <?php echo $form->textField($model,'direccion',array('placeholder'=>'Calle N° Población/Villa')); ?>
                <?php echo $form->error($model,'direccion'); ?>
            </div>
            
        </div>
        <div class="cuadro">
            <div class="row">
                <?php echo $form->labelEx($model,'region_id'); ?>
                <?php echo $form->dropDownList($model,'region_id', CHtml::listData(Regiones::model()->listar(), 'id', 'nombre'),
                        array(
                            'ajax' => array(
                                'type'=>'POST', //request type
                                'url'=>CController::createUrl('//regiones/llenaComuna'), 
                                'update'=>'#comuna_id', 
                            ),
                            'prompt'=>'Seleccione una Región')); ?>
                <?php echo $form->error($model,'region_id'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'comuna_id'); ?>
                <?php echo $form->dropDownList($model,'comuna_id',array(),
                        array(
                            'prompt'=>'Seleccione una Comuna',
                            'id'=>'comuna_id',
                        )
                    ); ?>
                <?php echo $form->error($model,'comuna_id'); ?>
            </div>


        </div>
        
        <div class="row buttons">
            <div class="boton_comprar_chico btn_aceptar">Guardar Cambios</div>
        </div>

    </div>    
<?php $this->endWidget(); ?>
</div><!-- form -->
