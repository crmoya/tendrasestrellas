<?php
/* @var $this PedidosController */
/* @var $model Pedidos */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pedidos-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Pedidos</h1>

<?php echo CHtml::link('Búsqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pedidos-grid',
	'dataProvider'=>$model->search(),
	'afterAjaxUpdate' => 'reinstallDatePicker',
	'filter'=>$model,
	'columns'=>array(
		array('name'=>'cliente','value'=>'$data->clientes->usuarios->nombre'),
		array(            
            'name'=>'fecha',
            'value'=>array($model,'gridDataColumn'),
			'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                        'model'=>$model, 
                                        'attribute'=>'fecha', 
                                        'language' => 'es',
                                        'htmlOptions' => array(
                                            'id' => 'datepicker_for_fecha',
                                            'size' => '10',
                                        ),
                                        'defaultOptions' => array(  // (#3)
                                            'showOn' => 'focus', 
                                            'dateFormat' => 'dd/mm/yy',
                                            'showOtherMonths' => true,
                                            'selectOtherMonths' => true,
                                            'changeMonth' => true,
                                            'changeYear' => true,
                                            'showButtonPanel' => true,
                                        )
                                    ), 
                                    true), 
        ),
                    'cantidad_productos',
                    array('name'=>'costo_despacho','value'=>'Tools::formateaPlata($data->costo_despacho)'),
                    array('name'=>'costo','value'=>'Tools::formateaPlata($data->costo)'),
                    array(  'name'=>'vigente',
                            'value'=>'Tools::formateaVigencia($data->vigente)',
                            'filter' => CHtml::listData( array(
                                                            array('id'=>'1','nombre'=>'TODOS'),
                                                        ), 'id', 'nombre'),
                            ),  
		array(
			'class'=>'CButtonColumn',
		),
	),
));

Yii::app()->clientScript->registerScript('re-install-date-picker', "
function reinstallDatePicker(id, data) {
	$('#datepicker_for_fecha').datepicker($.datepicker.regional[ 'es' ]);
}
");
