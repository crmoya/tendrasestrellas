<?php 
$cs=Yii::app()->clientScript;
$cs->registerCoreScript('jquery'); 
?>
<script type="text/javascript">
	$(function() {

		$('.fixed').change(function() {
			var text = $(this).val();
			text = text.replace(',','.');
			if(!isNaN(text)){
				var num = new Number(text);
				num = num.toFixed(0);
		   		$(this).val(num);
			}else{
				$(this).val(0);
			}
		});

		
		$('.cantidad').change(function(){
			var pedido_id = $(this).attr('pedido_id');
			var producto_id = $(this).attr('producto_id');
			var cantidad = $(this).val();
			$.ajax({
			  	type: "GET",
			  	url: '<?php echo CController::createUrl('pedidos/editar');?>',
			  	data: { pedido_id: pedido_id, producto_id: producto_id,cantidad: cantidad}
			}).done(function( msg ) {
				location.reload();
			});
		});
		
			
	});
</script>

<?php
/* @var $this PedidosController */
/* @var $model Pedidos */


$this->menu=array(
	array('label'=>'Administrar Pedidos', 'url'=>array('admin')),
);
?>

<h1>Editar Pedido #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		array('name'=>'cliente','value'=>$model->clientes->usuarios->nombre),
		array('name'=>'fecha','value'=>Tools::backFecha($model->fecha)),
		'cantidad_productos',
		array('name'=>'costo','value'=>Tools::formateaPlata($model->costo)),
		array('name'=>'metodo_envio','value'=>$model->metodos_envios->nombre),
		array('name'=>'costo_despacho','value'=>Tools::formateaPlata($model->costo_despacho)),
		array('name'=>'costo_total','value'=>Tools::formateaPlata($model->costo_despacho + $model->costo)),
	),
)); ?>
<br/>
<?php echo CHtml::link('Exportar a Excel',Yii::app()->baseUrl.'/pedidos/exportar/'.$model->id.'?nombre='.$model->clientes->usuarios->nombre.'&fecha='.Tools::backFecha($model->fecha)); ?>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?php echo CHtml::link('Agregar Producto a Pedido',Yii::app()->baseUrl.'/pedidos/agregar/'.$model->id); ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pedidos-grid',
	'dataProvider'=>$productos->searchPedido($model->id),
	'columns'=>array(
		'producto',
		array(
            'name'=>'cantidad',
            'type'=>'raw',
            'value'=>'CHtml::textField("cantidad[$data->cantidad]",$data->cantidad,array("class"=>"cantidad fixed","style"=>"width:50px;","pedido_id"=>"$data->pedido_id","producto_id"=>"$data->producto_id"))',
            'htmlOptions'=>array("width"=>"50px"),
        ),
		array('name'=>'precio','value'=>array($model,'plataPrecio')),
		array('name'=>'precio_por_mayor','value'=>array($model,'plataPrecioMayor')),
		'categoria',
		array(
		    'class'=>'CButtonColumn',
		    'template'=>'{eliminar}',
		    'buttons'=>array
		    (
		        'eliminar' => array
		        (
		            'label'=>'Eliminar producto',
		            'imageUrl'=>Yii::app()->request->baseUrl.'/images/eliminar.png',
		            'url'=>'Yii::app()->createUrl("//pedidos/eliminar?pedido_id=$data->pedido_id&producto_id=$data->producto_id")',
		        ),
		    ),
		),
	),
));
?>