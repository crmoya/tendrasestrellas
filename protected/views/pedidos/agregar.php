<?php
/* @var $this PedidosController */
/* @var $model Pedidos */


$this->menu=array(
	array('label'=>'Administrar Pedidos', 'url'=>array('admin')),
);
?>
<?php 
$cs=Yii::app()->clientScript;
$cs->registerCoreScript('jquery'); 
?>
<script type="text/javascript">
	$(function() {

		$('.fixed').change(function() {
			var text = $(this).val();
			text = text.replace(',','.');
			if(!isNaN(text)){
				var num = new Number(text);
				num = num.toFixed(0);
		   		$(this).val(num);
			}else{
				$(this).val(0);
			}
		});			
	});
</script>


<?php 

$cs=Yii::app()->clientScript;
$cs->registerCoreScript('jquery'); 

?>

<h1>Agregar Producto a Pedido #<?php echo $model->id; ?></h1>

<?php if(Yii::app()->user->hasFlash('pedidoMessage')): ?>
<div class="flash-success">
<?php echo Yii::app()->user->getFlash('pedidoMessage'); ?>
</div>
<?php endif; ?>
<?php if(Yii::app()->user->hasFlash('pedidoError')): ?>
<div class="flash-error">
<?php echo Yii::app()->user->getFlash('pedidoError'); ?>
</div>
<?php endif; ?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array('name'=>'cliente','value'=>$model->clientes->usuarios->nombre),
		array('name'=>'fecha','value'=>Tools::backFecha($model->fecha)),
		'cantidad_productos',
		array('name'=>'costo','value'=>Tools::formateaPlata($model->costo)),
		array('name'=>'metodo_envio','value'=>$model->metodos_envios->nombre),
		array('name'=>'costo_despacho','value'=>Tools::formateaPlata($model->costo_despacho)),
		array('name'=>'costo_total','value'=>Tools::formateaPlata($model->costo_despacho + $model->costo)),
	),
)); ?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pedido2-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($producto); ?>

	<table>
		<tr>
			<td>
				<?php echo $form->labelEx(Productos::model(),'categorias_id');?>
				<?php 
					echo CHtml::dropDownList('categoria_id','', 
							CHtml::listData(Categorias::model()->findAll(), 'id', 'nombre'),
							array(
								'prompt'=>'Seleccione Categoría',
								'ajax' => array(
									'type'=>'POST', //request type
									'url'=>CController::createUrl('categorias/getProductos'), 
									'update'=>'#producto_id', )
								)
					);
				?>
			</td>
			<td>
				<?php echo $form->labelEx(ProductosDePedido::model(),'producto');?>
				<?php 
					echo Chtml::dropDownList(
						'producto_id',
						'',
						CHtml::listData(Productos::model()->findAll(), 'id', 'nombre'),array('prompt'=>'Seleccione un Producto',)
					);
				?>
			</td>
			<td>
				<?php echo $form->labelEx(Pedidos::model(),'cantidad');?>
				<?php 
					echo Chtml::textField('cantidad','',array('class'=>'fixed'));
				?>
			</td>
		</tr>
	</table>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Agregar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->