<?php
/* @var $this PedidosController */
/* @var $model Pedidos */


$this->menu=array(
	array('label'=>'Administrar Pedidos', 'url'=>array('admin')),
);
?>

<h1>Ver Pedido #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		array('name'=>'cliente','value'=>$model->clientes->usuarios->nombre),
		array('name'=>'fecha','value'=>Tools::backFecha($model->fecha)),
		'cantidad_productos',
		array('name'=>'costo','value'=>Tools::formateaPlata($model->costo)),
		array('name'=>'metodo_envio','value'=>$model->metodos_envios->nombre),
		array('name'=>'costo_despacho','value'=>Tools::formateaPlata($model->costo_despacho)),
		array('name'=>'costo_total','value'=>Tools::formateaPlata($model->costo_despacho + $model->costo)),
	),
)); ?>
<br/>
<?php echo CHtml::link('Exportar a Excel','exportar/'.$model->id.'?nombre='.$model->clientes->usuarios->nombre.'&fecha='.Tools::backFecha($model->fecha)); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pedidos-grid',
	'dataProvider'=>$productos->searchPedido($model->id),
	'columns'=>array(
		'producto',
		'cantidad',
		array('name'=>'precio','value'=>array($model,'plataPrecio')),
		array('name'=>'precio_por_mayor','value'=>array($model,'plataPrecioMayor')),
		'categoria',
	),
));
?>