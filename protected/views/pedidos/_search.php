<?php
/* @var $this PedidosController */
/* @var $model Pedidos */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'clientes_id'); ?>
		<?php 
			echo $form->dropDownList(
				$model,
				'clientes_id',
				CHtml::listData(Clientes::model()->findAll(), 'id', 'nombre'),
				array('prompt'=>'Seleccione un Cliente',)
			);
		?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha'); ?>
		<?php
		$this->widget('zii.widgets.jui.CJuiDatePicker',
			array(
				'model'=>$model,
				'language' => 'es',
				'attribute'=>'fecha',
				// additional javascript options for the date picker plugin
				'options'=>array(
					'showAnim'=>'fold',
					'dateFormat'=>'dd/mm/yy',
					'changeYear'=>true,
					'changeMonth'=>true,
				),
				'htmlOptions'=>array(
			        'style'=>'width:70px;',
			    ),
			)
		);
		?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->