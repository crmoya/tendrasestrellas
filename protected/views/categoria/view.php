<?php
    $productos = Productos::model()->findAllByAttributes(array('categorias_id'=>$model->id),array('order'=>'id desc'));
    $destacado_id = ProductosDestacados::model()->destacadoDeCategoria($model->id);
    $destacado = Productos::model()->findByPk($destacado_id);
?>

<script>
$(document).ready(function(e){
    
    function arreglaAltosMiniFotos(){
        var max = 0;
        $('.gal_producto').each(function(e){
            var actual = $(this).height();
            if(actual > max){
                max = actual;
            }
        });
        $('.gal_producto').each(function(e){
            $(this).height(max);
        });
    }
    
    arreglaAltosMiniFotos();
    $(".categoria").each(function(e){
            $(this).removeClass("cat_selected");
            var id = $(this).attr("cat_id");
            if(id == '<?php echo $model->id;?>'){
                    $(this).addClass("cat_selected");
                    $(this).css("color",'#FEAA9D');
            }
    });
    $(".boton_comprar").click(function(e){
        var disponible = $('#no_disponible').html();
        if(disponible.trim() == '1'){
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->createUrl('//site/agregarPedido/');?>",
                data: { prod_id: <?php echo $destacado_id; ?>, cantidad: 1}
            }).done(function( msg ) {
                var agregados = msg;
                if(agregados > 0){
                    $(".cant_pedido").html('('+agregados+')');
                }
                window.location = '<?php echo Yii::app()->createUrl('//site/pedido/');?>';
            });
        }
        else{
            alert('No puede agregar este producto al pedido, pues no está disponible por el momento.');
        }        
    });

    redimensiona();
    function redimensiona(){
            var ancho_total = $(window).width()*0.7;
            if(ancho_total > 220){
                    var caben = Math.floor(ancho_total/220);
                    var total = caben*225;
                    $("#galeria").width(total);
            }else{
                    $("#galeria").width(240);
            }
    }
    $(window).resize(function(e){
            redimensiona();
    });
	
});
</script>
<div class="desc_cate">
	<div class="nueva_fila borde_fila">
		<a href="<?php echo CController::createUrl("//productos/$destacado_id");?>">
                    <?php $foto_id = 1;if($destacado != null)$foto_id = $destacado->getNextImagenId(); ?>
			<img class="imagen_cat no_clickeable" src="<?php echo Yii::app()->baseUrl.'/images/productos/'.$destacado_id.'/'.$foto_id.'.min'; ?>"/>
		</a>
	</div>	
	<div class="nueva_fila">
		<?php 
			
		?>
		<div class="inner tope_width">
			<p class="titulo-prod"><?php if($destacado != null) echo CHtml::encode($destacado->nombre); ?></p>
			<?php 
			if($destacado != null) echo CHtml::encode($destacado->descripcion);
			?><br/><br/>
                        <?php if($destacado->disponible == 0): ?>
                        <p class="titulo-prod-nodisp">NO DISPONIBLE</p>
                        <br/>
                        <?php endif; ?>
                        <div id="no_disponible" style="display:none;"><?php echo $destacado->disponible;?></div>
			<p class="precio_prod"><?php if($destacado != null) echo CHtml::encode(Tools::formateaPlata($destacado->precio)); ?></p>
			<div class="boton_comprar">Añadir al Pedido</div>
		</div>		
	</div>
	<div class="galeria" id="galeria">
		<?php 
		foreach($productos as $producto){
			if($producto!=null && $destacado != null){
                                $noDisponible = "";
                                if($producto->disponible == 0){
                                    $noDisponible = "<p class='nom_prod_nodisp'>NO DISPONIBLE</p>";
                                }
				if($producto->id != $destacado->id){
                                        $foto_id = $producto->getNextImagenId(); 
					echo '
					<a class="link_prod" href="'.CController::createUrl("//productos/$producto->id").'">
						<div class="gal_producto">
							<div class="img_borde">
								<img class="img_prod no_clickeable" src="'.Yii::app()->baseUrl."/images/productos/$producto->id/".$foto_id.".min".'"/>
                                                                        
							</div>
							<p class="nom_prod">'.CHtml::encode($producto->nombre).'</p>
                                                        '.$noDisponible.'
							<p class="nom_prod"><strong>'.CHtml::encode(Tools::formateaPlata($producto->precio)).'</strong></p>
						</div>
					</a>';	
				}
			}
		}
		?>
	</div>
</div>