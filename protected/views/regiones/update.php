<?php
/* @var $this RegionesController */
/* @var $model Regiones */


$this->menu=array(
	array('label'=>'Listar Regiones', 'url'=>array('index')),
	array('label'=>'Crear Región', 'url'=>array('create')),
	array('label'=>'Ver Región', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Regiones', 'url'=>array('admin')),
);
?>

<h1>Editar Región <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>