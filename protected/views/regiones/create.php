<?php
/* @var $this RegionesController */
/* @var $model Regiones */


$this->menu=array(
	array('label'=>'Listar Regiones', 'url'=>array('index')),
	array('label'=>'Administrar Regiones', 'url'=>array('admin')),
);
?>

<h1>Crear Región</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>