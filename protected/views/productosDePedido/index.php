<?php
/* @var $this ProductosDePedidoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Productos De Pedidos',
);

$this->menu=array(
	array('label'=>'Create ProductosDePedido', 'url'=>array('create')),
	array('label'=>'Manage ProductosDePedido', 'url'=>array('admin')),
);
?>

<h1>Productos De Pedidos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
