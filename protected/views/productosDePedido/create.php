<?php
/* @var $this ProductosDePedidoController */
/* @var $model ProductosDePedido */

$this->breadcrumbs=array(
	'Productos De Pedidos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProductosDePedido', 'url'=>array('index')),
	array('label'=>'Manage ProductosDePedido', 'url'=>array('admin')),
);
?>

<h1>Create ProductosDePedido</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>