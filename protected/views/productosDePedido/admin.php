<?php
/* @var $this ProductosDePedidoController */
/* @var $model ProductosDePedido */

$this->breadcrumbs=array(
	'Productos De Pedidos'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List ProductosDePedido', 'url'=>array('index')),
	array('label'=>'Create ProductosDePedido', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#productos-de-pedido-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Productos De Pedidos</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'productos-de-pedido-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'producto',
		'cantidad',
		'precio',
		'precio_por_mayor',
		'categoria',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
