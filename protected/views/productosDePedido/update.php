<?php
/* @var $this ProductosDePedidoController */
/* @var $model ProductosDePedido */

$this->breadcrumbs=array(
	'Productos De Pedidos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProductosDePedido', 'url'=>array('index')),
	array('label'=>'Create ProductosDePedido', 'url'=>array('create')),
	array('label'=>'View ProductosDePedido', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProductosDePedido', 'url'=>array('admin')),
);
?>

<h1>Update ProductosDePedido <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>