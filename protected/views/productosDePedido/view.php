<?php
/* @var $this ProductosDePedidoController */
/* @var $model ProductosDePedido */

$this->breadcrumbs=array(
	'Productos De Pedidos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ProductosDePedido', 'url'=>array('index')),
	array('label'=>'Create ProductosDePedido', 'url'=>array('create')),
	array('label'=>'Update ProductosDePedido', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ProductosDePedido', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProductosDePedido', 'url'=>array('admin')),
);
?>

<h1>View ProductosDePedido #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'producto',
		'cantidad',
		'precio',
		'precio_por_mayor',
		'categoria',
	),
)); ?>
