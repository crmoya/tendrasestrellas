<?php
/* @var $this MetodosEnvioController */
/* @var $model MetodosEnvio */

$this->menu=array(
	array('label'=>'Administrar Métodos de Envío', 'url'=>array('admin')),
);
?>

<h1>Crear Método de Envío</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>