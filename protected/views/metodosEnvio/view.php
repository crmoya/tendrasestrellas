<?php
/* @var $this MetodosEnvioController */
/* @var $model MetodosEnvio */

$this->menu=array(
	array('label'=>'Crear Método de Envío', 'url'=>array('create')),
	array('label'=>'Editar Método de Envío', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Eliminar Método de Envío', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Métodos de Envío', 'url'=>array('admin')),
);
?>

<h1>Ver Método de Envío #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nombre',
	),
)); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'metodos-envio-grid',
	'dataProvider'=>$costos->searchMetodo($model->id),
	'columns'=>array(
		array('name'=>'region','value'=>'$data->regiones->nombre'),
		array('name'=>'costo','value'=>array($model,'plataPrecio')),
	),
)); ?>
<br/>
<div style="text-align:center;">
	<img width="100" height="100" src="<?php echo CController::createUrl("//site/imagen",array('ruta'=>"metodos/$model->id")); ?>"/>
</div>
