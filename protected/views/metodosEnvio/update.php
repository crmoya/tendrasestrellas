<script type="text/javascript">
	$(function() {

		$('.fixed_costo').change(function() {
			var text = $(this).val();
			text = text.replace(',','.');
                        if(!isNaN(text)){
                            var num = new Number(text);
                            num = num.toFixed(0);
                            $(this).val(num);
                                
			}else{
				$(this).val(0);
			}
		});
		
		$('#guardar').click(function(){

			var send = "";
			$('.costo').each(function(){
				var regiones_id = $(this).attr('regiones_id');
				var metodos_envio_id = $(this).attr('metodos_envio_id');
				var costo = $(this).val();
				send += regiones_id+"_"+metodos_envio_id+"_"+costo+";";
			});

			$.ajax({
			  	type: "POST",
			  	url: '<?php echo CController::createUrl('//costos/editar');?>',
			  	data: { data: send}
			}).done(function( msg ) {
                            	$('#costo-form').submit();
			});	
			return false;
		});
		
			
	});
</script>
<?php
/* @var $this MetodosEnvioController */
/* @var $model MetodosEnvio */

$this->menu=array(
	array('label'=>'Crear Método de Envío', 'url'=>array('create')),
	array('label'=>'Ver Método de Envío', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Métodos de Envío', 'url'=>array('admin')),
);
?>

<h1>Editar Método de Envío <?php echo $model->id; ?></h1>
<?php if(Yii::app()->user->hasFlash('costoMessage')): ?>
<div class="flash-success">
<?php echo Yii::app()->user->getFlash('costoMessage'); ?>
</div>
<?php endif; ?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'costo-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'imagen'); ?>
		<?php echo $form->fileField($model,'imagen'); ?>
		<?php echo $form->error($model,'imagen'); ?>
	</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'metodos-envio-grid',
	'dataProvider'=>$costos->searchMetodo($model->id),
	'columns'=>array(
		array('name'=>'region','value'=>'$data->regiones->nombre'),
		array(
				'name'=>'costo',
				'type'=>'raw',
				'value'=>'CHtml::textField("costo[$data->costo]",$data->costo,array("class"=>"costo fixed_costo","style"=>"width:50px;","regiones_id"=>"$data->regiones_id","metodos_envio_id"=>"$data->metodos_envio_id"))',
				'htmlOptions'=>array("width"=>"50px"),
		),
	),
)); ?>

<div style="text-align:center;">
	<img width="100" height="100" src="<?php echo CController::createUrl("//site/imagen",array('ruta'=>"metodos/$model->id")); ?>"/>
</div>
<div class="row buttons">
	<?php echo CHtml::submitButton('Guardar',array('id'=>'guardar')); ?>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->