<?php
/* @var $this MetodosEnvioController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Metodos Envios',
);

$this->menu=array(
	array('label'=>'Create MetodosEnvio', 'url'=>array('create')),
	array('label'=>'Manage MetodosEnvio', 'url'=>array('admin')),
);
?>

<h1>Metodos Envios</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
