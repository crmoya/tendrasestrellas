<?php
/* @var $this ProductosDestacadosController */
/* @var $model ProductosDestacados */

$this->breadcrumbs=array(
	'Productos Destacadoses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProductosDestacados', 'url'=>array('index')),
	array('label'=>'Create ProductosDestacados', 'url'=>array('create')),
	array('label'=>'View ProductosDestacados', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProductosDestacados', 'url'=>array('admin')),
);
?>

<h1>Update ProductosDestacados <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>