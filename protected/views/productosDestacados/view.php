<?php
/* @var $this ProductosDestacadosController */
/* @var $model ProductosDestacados */

$this->breadcrumbs=array(
	'Productos Destacadoses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ProductosDestacados', 'url'=>array('index')),
	array('label'=>'Create ProductosDestacados', 'url'=>array('create')),
	array('label'=>'Update ProductosDestacados', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ProductosDestacados', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProductosDestacados', 'url'=>array('admin')),
);
?>

<h1>View ProductosDestacados #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'productos_id',
	),
)); ?>
