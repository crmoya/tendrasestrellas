<?php
/* @var $this ProductosDestacadosController */
/* @var $data ProductosDestacados */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('productos_id')); ?>:</b>
	<?php echo CHtml::encode($data->productos_id); ?>
	<br />


</div>