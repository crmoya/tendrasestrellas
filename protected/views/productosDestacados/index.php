<?php
/* @var $this ProductosDestacadosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Productos Destacadoses',
);

$this->menu=array(
	array('label'=>'Create ProductosDestacados', 'url'=>array('create')),
	array('label'=>'Manage ProductosDestacados', 'url'=>array('admin')),
);
?>

<h1>Productos Destacadoses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
