<?php
/* @var $this ProductosDestacadosController */
/* @var $model ProductosDestacados */

$this->breadcrumbs=array(
	'Productos Destacadoses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProductosDestacados', 'url'=>array('index')),
	array('label'=>'Manage ProductosDestacados', 'url'=>array('admin')),
);
?>

<h1>Create ProductosDestacados</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>